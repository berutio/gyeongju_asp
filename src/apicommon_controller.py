# -*- coding: utf-8 -*-
import logging
import urllib
import urllib2
import json
from google.appengine.api import urlfetch


SEARCH_API_URL = 'http://160.16.77.224/joinpos/abc.php'
SEARCH_API_KEY = 'wPK1Yys8PMvbNaolQn2t'

'''
params = urllib.urlencode({'hoge':1, 'fuga':2})
f = urllib.urlopen(url, params)
'''
def search(query_args, search_url=SEARCH_API_URL):
    urlfetch.set_default_fetch_deadline(60)
    try:
        if query_args:
            response = urllib2.urlopen(search_url, urlencode(query_args)) #POST
        else:
            response = urllib2.urlopen(search_url) #GET
        return response.read()
    except urllib2.HTTPError, e:
        logging.error('HTTPError: %d', e.code)
        raise
    except urllib2.URLError, e:
        logging.error('URLError: %s', e.reason)
        raise
    except Exception:
        logging.error('Unknown Exception.')
        raise


def urlencode(query_args):
    str_query_args = {}
    for k, v in query_args.iteritems():
        str_query_args[k] = unicode(v).encode('utf-8')
    return urllib.urlencode(str_query_args)
