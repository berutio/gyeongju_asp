# -*- coding: utf-8 -*-
from google.appengine.api import users
from google.appengine.ext import db, webapp
from google.appengine.ext.webapp import template
from google.appengine.api import mail

from posmodel import *
#from test.test_itertools import prod
webapp.template.register_template_library('filters.filter')

import os
import csv
from StringIO import StringIO
#LOCAL

#DiretoryInfo
dir_name  = "setup"
slash_dir = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"


'''ajax_controllerにも同じFunction存在
'''
def func_shop_section(obj_shop):
    query_data = SectionTable.all().filter('shop_id =', obj_shop.shop_id)
    data = []
    for section in query_data:
        data.append(section.food_drink)
    obj_shop.it_section_kbn = data
    obj_shop.put()
    return

'''
'''
class ShopList(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            obj_comp = get_mem_company(shop_id[:6])

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('setup', lang_sel),
                    'title':"SHOP_APPEND_DELETE",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'company_name':obj_comp.kname,
                }
            self.helpDispatch("primary", 'setup_shoplist.html', template_vals)
        except UserException as e:
            occur_point = "%s::ShopList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ShopList::"%ctrl_name
            func_error_display(self, occur_point + e.message)

'''
'''
class ShopEdit(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'G_LANG_SEL':G_LANG_SEL,
                    'control':translate('setup', lang_sel),
                    'title':translate('shopedit', lang_sel) + translate('setup', lang_sel),
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'setup_shop.html', template_vals)
        except UserException as e:
            occur_point = "%s::ShopEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ShopEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise

    def post(self):
        try:
            loginData = self.logincheck()
            shop_id  = self.request.get('shop_id')
            obj_shop = get_mem_shop(shop_id)

            #obj_shop.regi_version = "2.7.7"
            st_moneytitle = self.request.get_all('pay_name')
            obj_shop.st_moneytitle[2] = st_moneytitle[0]
            obj_shop.st_moneytitle[0] = st_moneytitle[1]
            if len(st_moneytitle) > 2:
                obj_shop.st_moneytitle[1] = st_moneytitle[2]
                obj_shop.st_moneytitle[3] = st_moneytitle[3]
            else:
                obj_shop.st_moneytitle[1] = u"商品券"
                obj_shop.st_moneytitle[3] = u"売掛"

            obj_shop.time_open  = int(self.request.get('time_open'))
            obj_shop.time_close = int(self.request.get('time_close'))
            obj_shop.it_time_area    = [0,0,0]
            obj_shop.it_time_area[0] = int(self.request.get('time_cnt'))
            obj_shop.it_time_area[1] = int(self.request.get('time_mid1', 0))
            obj_shop.it_time_area[2] = int(self.request.get('time_mid2', 0))
            st_time_name = self.request.get_all('time_name')
            obj_shop.st_time_name = st_time_name
            #
            st_shopbase = self.request.get_all('shop_base')
            #logging.debug("setup_controller::ShopEdit:: shop_base=%s" % st_shopbase)
            obj_shop.kname     = st_shopbase[0]  #ShopName
            obj_shop.zipcode   = st_shopbase[1]  #郵便番号
            obj_shop.address1  = st_shopbase[2]  #住所１
            obj_shop.address2  = st_shopbase[3]  #住所２
            #obj_shop.address3  = st_shopbase[4]  #住所3
            obj_shop.telno     = st_shopbase[5]  #電話番号
            obj_shop.faxno     = st_shopbase[6]  #FAX番号
            #obj_shop.admin_id  = st_shopbase[7]  #管理者
            obj_shop.etc_taxcode= st_shopbase[8]  #
            obj_shop.hasu_kbn  = int(st_shopbase[9])  #
            login_check = int(st_shopbase[10])  #
            if login_check == 1:
                obj_shop.login_check = True
            else:
                obj_shop.login_check = False
            obj_shop.login_check_everytime = int(st_shopbase[11])  #
            obj_shop.regi_junbikin_kbn = int(st_shopbase[12])  #
            obj_shop.table_limit_time  = int(st_shopbase[13])
            #obj_shop.otoosi_kbn        = int(st_shopbase[14])

            #--------------------------------------------------------------------
            obj_shop.credit_card = self.request.get_all('credit')  #クレジットカード種類
            obj_shop.emoney_card = self.request.get_all('emoney')  #E-Money種類
            obj_shop.svc_ticket  = self.request.get_all('service') #サービス券種類
            #
            temp_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            temp_list = self.request.get_all('svc_kbn') #値引・割引区分
            for idx, item in enumerate(temp_list):
                temp_list[idx] = int(item)
            obj_shop.svc_kbn = temp_list
            #
            temp_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            temp_list = self.request.get_all('svc_amount') #サービス券種類
            for idx, item in enumerate(temp_list):
                temp_list[idx] = int(item)
            obj_shop.svc_amount = temp_list
            #---
            obj_shop.plus_ticket  = self.request.get_all('plus') #割増券種類
            temp_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            temp_list = self.request.get_all('plus_kbn') #割増区分
            for idx, item in enumerate(temp_list):
                temp_list[idx] = int(item)
            obj_shop.plus_kbn = temp_list
            temp_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            temp_list = self.request.get_all('plus_amount') #割増金額
            for idx, item in enumerate(temp_list):
                temp_list[idx] = int(item)
            obj_shop.plus_amount = temp_list
            #---
            obj_shop.urikake_card   = self.request.get_all('urikake')   #Urikake種類
            obj_shop.gift_card      = self.request.get_all('gift_card')
            obj_shop.cost_advol     = self.request.get_all('advol')     #宣伝広告費名  MAX=10
            obj_shop.cost_kounetuhi = self.request.get_all('kounetuhi') #光熱費        MAX=5
            obj_shop.cost_overhead  = self.request.get_all('overhead')  #営業経費      MAX=10
            obj_shop.cost_labor     = self.request.get_all('labor')     #人件費        MAX=5
            obj_shop.cash_io        = self.request.get_all('smallcash') #小口現金項目  MAX=10
            if not obj_shop.cash_io:
                obj_shop.cash_io = ["","","","","","","","","",""]

            obj_shop.print_receipt  = self.request.get_all('receipt')   #レシート設定内容     MAX=3行
            obj_shop.print_receipt_tail  = self.request.get_all('receipt_tail')   #レシート設定内容     MAX=3行
            if not obj_shop.print_receipt_tail:
                obj_shop.print_receipt_tail = ["", "", "", "", "", ""]
            obj_shop.print_tadasi   = self.request.get('tadasi')        #領収書の但し書き
            food_printer_cnt = int(self.request.get('printer_cnt'))
            lang_sel = loginData['shop_user'].lang_sel
            if food_printer_cnt == 1:
                obj_shop.print_usage = [trans_unicode('print_usage_1', lang_sel),trans_unicode('print_usage_2', lang_sel),trans_unicode('print_usage_3', lang_sel)]
            if food_printer_cnt == 2:
                obj_shop.print_usage = [trans_unicode('print_usage_1', lang_sel),trans_unicode('print_usage_2', lang_sel),trans_unicode('print_usage_3', lang_sel) + "_1",trans_unicode('print_usage_3', lang_sel) + "_2"]
            if food_printer_cnt == 3:
                obj_shop.print_usage = [trans_unicode('print_usage_1', lang_sel),trans_unicode('print_usage_2', lang_sel),trans_unicode('print_usage_3', lang_sel) + "_1",trans_unicode('print_usage_3', lang_sel) + "_2",trans_unicode('print_usage_3', lang_sel) + "_3"]
            #--
            obj_shop.order_option = self.request.get_all('order_option')  #オーダーオプション
            #
            warn_limit = [0, 0, 0, 0, 0, 0]
            warn_list = self.request.get_all('shop_warn') #警告
            for idx, item in enumerate(warn_list):
                warn_limit[idx] = int(item)
                #logging.error("setup_controller::ShopEdit:: warn_limit=%d, %d" % (idx, warn_limit[idx]))
            obj_shop.it_warn = warn_limit
            #--
            obj_shop.st_custom_option = self.request.get_all('shop_option') #警告
            obj_shop.st_menu_protect  = self.request.get_all('check_menu')
            obj_shop.st_tab_group = self.request.get_all('tabgroup')
            obj_shop.st_order_msg = self.request.get_all('shop_order_msg') #
            obj_shop.service_rate = int(self.request.get('serviceRate', '0')) #
            #
            obj_shop.put()
            del_mem_shop(shop_id)

            diable_download = self.request.get('disableDownloadDate') #
            ShopFlagInfo.insert_or_update(shop_id = shop_id, diable_download = diable_download)
            self.redirect(slash_dir + "shopedit?shop_id=%s" % (shop_id))
        except UserException as e:
            occur_point = "%s::TouchMenuDetail::"%ctrl_name
            func_error_display(self, occur_point + e.value)
            raise
        except Exception as e:
            occur_point = "%s::TouchMenuDetail::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise

    def account_check(self, obj_shop, account):
        obj_user = ShopUser.get_by_key_name(account)
        if obj_user:
            if obj_shop.shop_id == obj_user.shop_id:
                return True
            else:
                logging.error('setup_controller::account_check:: already asigned account shop=%s acount=%s'%(obj_user.shop_id, account))
                return False
        if obj_shop:
            if obj_shop.admin_id:
                old_user = ShopUser.get_by_key_name(obj_shop.admin_id)
                if old_user: old_user.delete()
            #ShopUser追加
            ShopUser.insert_or_fail(
                        shop_id     = obj_shop.shop_id,
                        email_id    = account,
                        role_flag   = 1
                      )
        return True


'''
'''
class ShopSeat(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
        #if not obj_shop.group_name:
        #    obj_shop.group_name = ["Group A", "Group B", "Group C", "Group D", "Group E"]
        if not obj_shop.seat_man:
            tmp_int_array = []
            tmp_str_array = []
            for tmp in obj_shop.seat_code:
                tmp_int_array.append(0)
                tmp_str_array.append("")
            obj_shop.seat_gid  = tmp_int_array
            obj_shop.seat_man  = tmp_int_array
            obj_shop.seat_memo = tmp_str_array

            obj_shop.seat_type = tmp_int_array
            obj_shop.seat_x    = tmp_int_array
            obj_shop.seat_y    = tmp_int_array

        if  obj_shop.it_setup[8]:
            if obj_shop.seat_type:
                seat_zip = zip(obj_shop.seat_gid, obj_shop.seat_code, obj_shop.seat_man, obj_shop.seat_memo, obj_shop.seat_type, obj_shop.seat_x, obj_shop.seat_y)
            else:
                seat_zip = zip(obj_shop.seat_gid, obj_shop.seat_code, obj_shop.seat_man, obj_shop.seat_memo, obj_shop.seat_man, obj_shop.seat_man, obj_shop.seat_man)
        else:
            seat_zip = zip(obj_shop.seat_gid, obj_shop.seat_code, obj_shop.seat_man, obj_shop.seat_memo)

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':lang_sel,
                'control':translate('setup', lang_sel),
                'title':translate('table', lang_sel) + translate('setup', lang_sel),
                'loginData':loginData,
                'shop_id': shop_id,
                'shops': shops,
                'shop':obj_shop,
                'seat_zip':seat_zip,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
            }
        self.helpDispatch("primary", 'setup_seat.html', template_vals)

    def post(self):
        try:
            shop_id  = self.request.get('shop_id')
            obj_shop = get_mem_shop(shop_id)

            obj_shop.group_name = self.request.get_all('group_name')
            obj_shop.seat_memo  = self.request.get_all('seat_memo')
            obj_shop.seat_code  = self.request.get_all('seat_code')
            obj_shop.max_seat   = len(obj_shop.seat_code)
            temp_array  = self.request.get_all('seat_gid')
            int_array = []
            for gid in temp_array:
                int_array.append(int(gid))
            obj_shop.seat_gid = int_array
            temp_array = self.request.get_all('seat_man')
            int_array = []
            for gid in temp_array:
                int_array.append(int(gid))
            obj_shop.seat_man = int_array
            #---
            if  obj_shop.it_setup[8]:
                temp_array = self.request.get_all('seat_type')
                int_array = []
                for gid in temp_array:
                    int_array.append(int(gid))
                obj_shop.seat_type = int_array

                temp_array = self.request.get_all('seat_x')
                int_array = []
                for gid in temp_array:
                    int_array.append(int(gid))
                obj_shop.seat_x = int_array

                temp_array = self.request.get_all('seat_y')
                int_array = []
                for gid in temp_array:
                    int_array.append(int(gid))
                obj_shop.seat_y = int_array

            obj_shop.put()
            del_mem_shop(shop_id)
            ShopFlagInfo.insert_or_update(shop_id = shop_id)
            self.redirect(slash_dir + "shopseat?shop_id=%s" % (shop_id))
        except Exception as e:
            logging.error('intro_controller::QnaBoardEdit Exception ERROR=' + e.message)
            raise



'''
'''
class ShopSeatLayout(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
        seat_zip = zip(obj_shop.seat_gid, obj_shop.seat_code, obj_shop.seat_man, obj_shop.seat_memo, obj_shop.seat_type, obj_shop.seat_x, obj_shop.seat_y)

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':lang_sel,
                'control':translate('setup', lang_sel),
                'title':translate('table_layout', lang_sel),
                'loginData':loginData,
                'shop_id': shop_id,
                'shops': shops,
                'shop':obj_shop,
                'seat_zip':seat_zip,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
            }
        self.helpDispatch("primary", 'setup_seat_layout.html', template_vals)



class CsvUploader(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':lang_sel,
                'control':translate('setup', lang_sel),
                'title':"CSVアップロード",
                'loginData':loginData,
                'shop_id': shop_id,
                'shops': shops,
                'shop':obj_shop,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month
            }
        self.helpDispatch("primary", 'csv_read.html', template_vals)

    def post(self):
        shop_id  = self.request.get('shop_id')
        file_kbn = self.request.get('file_kbn')
        rawfile  = self.request.get('file')
        csvfile  = csv.reader(StringIO(rawfile))  #csvfile = csv.reader(StringIO(rawfile), delimiter=',')

        obj_shop = get_mem_shop(shop_id)

        #-------------------------------
        if file_kbn == '1': #部門
            obj_shop.lastno_sect = 1
            obj_shop.put()
            del_mem_shop(shop_id)
            db.delete(SectionTable.all().filter("shop_id =", shop_id))  #5

            for idx, row in enumerate(csvfile):
                if unicode(row[2],'cp932') == u"ドリンク":
                    food_drink = 1
                else:
                    food_drink = 2

                obj_sect = SectionTable.insert_or_fail(
                                shop_id     = shop_id,
                                kname       = unicode(row[1],'cp932'),
                                priority    = int(row[3]),
                                lastno_prod = int(row[4]),
                                food_drink  = food_drink
                            )
            func_shop_section(obj_shop)
        #-------------------------------
        if file_kbn == '2': #オプション
            obj_shop.lastno_option = 1
            obj_shop.put()
            del_mem_shop(shop_id)
            db.delete(OptionTable.all().filter("shop_id =", shop_id))   #6

            for idx, row in enumerate(csvfile):
                for i, item in enumerate(row):
                    if item: row[i] = unicode(item,'cp932')
                if len(row) < 14:
                    st_option = [row[2],row[3],row[4],row[5],row[6],row[7]]
                else:
                    st_option = [row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13]]

                obj_sect = OptionTable.insert_or_fail(
                                shop_id   = shop_id,
                                kname     = row[1],
                                st_option = st_option
                            )
        #-------------------------------
        if file_kbn >= '3': #3:新規、4:追加、5:新規（タッチメニュー修正なし）
            if file_kbn == '3':
                #TouchMenu Clear
                q  = TouchMenuTable.all().filter('shop_id  =', shop_id)
                for item in q:
                    item.st_plucode  = ["" for j in range(24)]
                    item.st_dispname = ["" for j in range(24)]
                    item.st_dispname2= ["" for j in range(24)]
                    item.it_position = [0  for j in range(24)]
                    item.it_color    = [0  for j in range(24)]
                    item.put()
            if file_kbn == '3' or file_kbn == '5':
                db.delete(ProductTable.all().filter("shop_id =", shop_id))  #

            for idx, row in enumerate(csvfile):
                price_unit = 0
                price_raw  = 0
                changeable = 0
                course_cd  = ""
                option_tabid = 0
                st_option_id = ["","",""]
                if row[4]: price_unit = int(row[4])
                if row[5]: price_raw  = int(row[5])
                if row[6]: st_option_id[0] = "%02d"%int(row[6])
                if row[7]: st_option_id[1] = "%02d"%int(row[7])
                if row[8]: st_option_id[2] = "%02d"%int(row[8])
                if unicode(row[9],'cp932') == u"可": changeable = 1
                if len(row) > 10:
                    if row[10]: option_tabid  = int(row[10])

                if int(row[1]) > 99:
                    return

                obj_prod = ProductTable.insert_batch(
                                shop_id    = shop_id,
                                sect_id    = "%02d"%int(row[0]),
                                seq_no     = int(row[1]),
                                kname      = unicode(row[2],'cp932'),
                                kname2     = unicode(row[3],'cp932'),
                                price_unit = price_unit,
                                price_raw  = price_raw,
                                st_option_id = st_option_id,
                                changeable   = changeable,
                                course_cd    = course_cd,
                                option_tabid = option_tabid
                            )
        '''
        for idx, row in enumerate(csvfile):
            tmp = ""
            for item in row:
                tmp = tmp + unicode(item, 'cp932') + ", "
            logging.error("CsvUploader:: Line=%02d, row_str=%s" %(idx, tmp))
        '''
        self.redirect(slash_dir + "csvread")


'''
'''
class ProductCsvUpload(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            error = self.request.get('err')

            if self.request.get('csv'):
                #MasterController ProductEditと同じ
                now = getJST()
                self.response.headers['Content-Type'] = "application/x-csv; charset=Shift_JIS"
                self.response.headers['Content-Disposition'] = "attachment; filename=product_"+now.strftime("%Y%m%d")+".csv"
                title = u"部門コード,商品コード,商品名(上段）,商品名(下段）,小売単価,原価,予約単価"
                self.response.out.write(title  + "\r\n")

                query = ProductTable.all().filter("shop_id =", shop_id)
                for record in query:
                    if record.flag_del: continue

                    #logging.error('ProductEdit::plucode=%s, kname=%s, kname2=%s' %( record.plucode, record.kname, record.kname2 ))
                    item = ["","","","","","","0"]
                    item[0] = record.plucode[:2]
                    item[1] = record.plucode[2:]
                    item[2] = record.kname.encode('CP932').decode('SJIS')
                    item[3] = record.kname2.encode('CP932').decode('SJIS')
                    item[4] = str(record.price_unit)
                    item[5] = str(record.price_raw)
                    if record.price_reserved:
                        item[6] = str(record.price_reserved)
                    self.response.out.write(','.join(item) + "\r\n")
                return

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('setup', lang_sel),
                    'title':"商品データアップロード",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month
                    'csvdownload':2, #CSVダウンロードボタン追加
                    'error':error,
                }
            self.helpDispatch("primary", 'master_product_csv.html', template_vals)
        except UserException as e:
            occur_point = "%s::ProductEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ProductEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            logging.error('%s::ProductEdit::plucode=%s, kname=%s%s' %(ctrl_name, record.plucode, record.kname, record.kname2 ))
            raise

    def post(self):
        shop_id  = self.request.get('shop_id')
        rawfile  = self.request.get('file')
        csvfile  = csv.reader(StringIO(rawfile))  #csvfile = csv.reader(StringIO(rawfile), delimiter=',')

        obj_shop = get_mem_shop(shop_id)
        #Error Check
        found_error = False
        for idx, row in enumerate(csvfile):
            if idx == 0: continue
            if not row[0].isdigit():
                found_error = True
                break
            if not row[1].isdigit():
                if row[1] != "a":
                    found_error = True
                    break
            if not row[2]:
                found_error = True
                break
            if not row[4].isdigit():
                found_error = True
                break
            if not row[5].isdigit():
                found_error = True
                break

        if found_error:
            now = getJST()
            self.response.headers['Content-Type'] = "application/x-csv; charset=Shift_JIS"
            self.response.headers['Content-Disposition'] = "attachment; filename=product_"+now.strftime("%Y%m%d")+"ERROR.csv"
            title = u"部門コード,商品コード,商品名(上段）,商品名(下段）,小売単価,原価,予約単価"
            self.response.out.write(title  + "\r\n")

            csvfile  = csv.reader(StringIO(rawfile))  #csvfile = csv.reader(StringIO(rawfile), delimiter=',')
            for idx, row in enumerate(csvfile):
                if idx == 0: continue
                if not row[0].isdigit():
                    row[0] += "[ERROR]"
                if not row[1].isdigit():
                    if row[1] != "a":
                        row[1] += "[ERROR]"
                if not row[2]:
                    row[2] += "[ERROR]"
                if not row[4].isdigit():
                    row[4] += "[ERROR]"
                if not row[5].isdigit():
                    row[5] += "[ERROR]"
                if not row[6].isdigit():
                    row[6] += "[ERROR]"
                self.response.out.write(','.join(row) + "\r\n")
            return

        ####DB反映
        csvfile = csv.reader(StringIO(rawfile))  #csvfile = csv.reader(StringIO(rawfile), delimiter=',')
        for idx, row in enumerate(csvfile):
            if idx == 0: continue
            plucode = ""
            price_unit = 0
            price_raw  = 0
            changeable = 0
            st_option_id = ["","",""]
            if row[1] != "a": #APPEND
                plucode = "%02d%02d" % (int(row[0]), int(row[1]))
            kname  = unicode(row[2],'cp932')
            kname2 = unicode(row[3],'cp932')
            price_unit = int(row[4])
            price_raw  = int(row[5])
            #price_reserved = 0
            #if row[6]:
            #    price_reserved = int(row[6])
            if plucode:
                obj_prod = ProductTable.get_by_key_name(shop_id + plucode)
                if not obj_prod:
                    logging.error("setup_controller::ProductCsvUpload::not found shop=%s, plucode=%s" %(shop_id,plucode))
                    return
                if obj_prod.kname != kname or obj_prod.kname2 != kname2:
                    obj_prod.kname  = kname
                    obj_prod.kname2 = kname2
                    self.TouchMenuTableUpdate(obj_prod, obj_shop)
                obj_prod.price_unit = price_unit
                obj_prod.price_raw  = price_raw
                #obj_prod.price_reserved = price_reserved
                obj_prod.put()
            else:
                obj_prod = ProductTable.insert_or_fail(
                        shop_id    = shop_id,
                        sect_id    = "%02d"%int(row[0]),
                        kname      = kname,
                        kname2     = kname2,
                        price_unit = price_unit,
                        price_raw  = price_raw,
                        st_option_id = st_option_id,
                        changeable   = changeable
                    )
            if not obj_prod:
                logging.error("setup_controller::ProductCsvUpload::ProductTable error shop=%s, plucode=%s" %(shop_id,plucode))

        self.redirect("/master/productall")
        return
    '''
    '''
    def TouchMenuTableUpdate(self, obj_prod, obj_shop):
        menus = obj_shop.touchmenutable_set
        for item in menus:
            if not item.kname: continue
            change_flag = 0
            for idx, plucode in enumerate(item.st_plucode):
                if not plucode: continue
                if plucode != obj_prod.plucode: continue
                item.st_dispname[idx]  = obj_prod.kname
                item.st_dispname2[idx] = obj_prod.kname2
                change_flag = 1
            if change_flag:
                item.put()

'''
'''
class AspReceipt(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)


            receipt_no = self.request.get('receipt_no')
            flag       = self.request.get('flag')
            if receipt_no and flag:
                if flag == "0":
                    '''領収書発行
                    '''
                    obj_asp = AspReceiptTable.get_by_key_name(receipt_no)
                    #obj_asp.confirm_day = "20130729"
                    pdf_function.create_receipt(self, obj_shop.kname, obj_asp.seikyu_no, obj_asp.amount, obj_asp.tax, obj_asp.from_month, obj_asp.to_month, obj_asp.confirm_day )
                    return
                if flag == "1":
                    '''振込済み
                    '''
                    obj_asp = AspReceiptTable.get_by_key_name(receipt_no)
                    now = getJST()
                    obj_asp.confirm_day = now.strftime("%Y%m%d")
                    obj_asp.put()
                    obj_shop.asp_hurikomi = 1
                    obj_shop.put()
                    self.send_email_forinput(obj_shop)
                    del_mem_shop(shop_id)


            query_data = []
            query = AspReceiptTable().all().filter("shop_id =", shop_id)
            for item in query:
                tmp_record = ["", 0, 0, "", "", ""]
                tmp_record[0] = item.seikyu_no
                tmp_record[1] = item.amount + item.tax
                tmp_record[2] = item.tax
                tmp_record[3] = item.from_month
                tmp_record[4] = item.to_month
                tmp_record[5] = item.confirm_day
                query_data.append(tmp_record)


            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('setup', lang_sel),
                    'title':"ASP費用履歴",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'items':query_data,
                    'shop':obj_shop,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'report_asp.html', template_vals)
        except UserException as e:
            occur_point = "%s::AspReceipt::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::AspReceipt::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise

    def send_email_forinput(self, obj_shop):
        message = mail.EmailMessage(sender  = "berutio@gmail.com",
                                    subject = u"ASP振込完了 for " + obj_shop.kname)
        message.to = "berutio@gmail.com"
        message.body = "completed a bank transfer for " + obj_shop.kname
        message.send()

'''
Journal download
'''
class CsvDownload(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id = loginData['shop_user'].shop_id
        month   = self.request.get('month')

        self.response.headers['Content-Type'] = "application/x-csv; charset=Shift_JIS"
        self.response.headers['Content-Disposition'] = "attachment; filename=receipt_detail.csv"

        #title = u"receipt_no, type, total, product_name, unit_price, quantity, sum"
        #self.response.out.write(title  + "\r\n")
        cnt = 1
        for idx in range(1,32):
            query_data = ReceiptData.all().filter('service_date =', "%s%02d"%(month, idx) ).filter('shop_id =', shop_id)
            query_data.filter('type = ', 1)
            for item in query_data:
                product_zip = zip(item.st_product_name, item.it_unit_price, item.it_quantity, item.it_sum)
                for pd in product_zip:
                    #logging.debug("setup_controller::CsvDownload::%s,%d,%d,%s,%d,%d,%d\r\n" % (item.receipt_no, item.type, item.total, pd[0], pd[1], pd[2], pd[3]))
                    self.response.out.write("%d,%s,%d,%s,%d,%d,%d\r\n" % (cnt, item.receipt_no, item.total, pd[0].replace(u"～", ""), pd[1], pd[2], pd[3]))
                    cnt = cnt + 1
'''
'''
class HistoryClear(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id = loginData['shop_user'].shop_id
        month   = self.request.get('month')
        memcache.flush_all()
        self.delete_history(shop_id, month) #

    @classmethod
    def delete_history(self, shop_id, month):
        entity = ShopInfo.get_by_key_name(shop_id)
        if not entity:
            logging.error("index_controller::HistoryClear:: not found ShopInfo=%s", shop_id)
            return False
        ymd = "%s01"%month
        #---------履歴データ
        db.delete( ReceiptData.all().filter(       "shop_id =", shop_id).filter("service_date <", ymd))
        db.delete( DailySimeData.all().filter(     "shop_id =", shop_id).filter("date <",ymd))
        db.delete( AuditTable.all().filter(        "shop_id =", shop_id).filter("ymd <", ymd))
        db.delete( DailyUriage.all().filter(       "shop_id =", shop_id).filter("ymd <", ymd))
        #---
        db.delete( MonthlyUriage.all().filter(     "shop_id =", shop_id).filter("yyyymm <", month))
        db.delete( AuditSumTable.all().filter(     "shop_id =", shop_id).filter("month <", month))

        #db.delete( ProductMonthlyUriage.all().filter("shop_id =", shop_id).filter("year <", month)) #13  年間(year < y)
        #----
        #db.delete( TimecardMonthly.all().filter("shop_id =", shop_id).filter("ymd <", ymd))      #16  月間(year_month < ym)
        #db.delete( StaffDailyTime.all().filter("shop_id =", shop_id).filter("ymd <", ymd))       #17  日間(ymd < ymd)
        #db.delete( CalendarData.all().filter("shop_id =", shop_id).filter("ymd <", ymd))         #18  月間(year_month < ym)
        ####
        #db.delete( ShiftMonthlyStaff.all().filter("shop_id =", shop_id).filter("ymd <", ymd))    #18  月間(year_month < ym)
        #db.delete( SiireDaily.all().filter("shop_id =", shop_id).filter("ymd <", ymd))           #17  日間(ymd < ymd)
        #db.delete( SiireMonthly.all().filter("shop_id =", shop_id).filter("ymd <", ymd))         #17  月間(yyyymm < ym)
        #db.delete( SiireYearly.all().filter("shop_id =", shop_id).filter("ymd <", ymd))          #17  年間(year < y)
        #
        return True


'''
Journal download
'''
class MasterCopy(Helper):
    def get(self):
        pass

############
class TouchMenuTableExpand(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
        obj_shop.maxtab_touch += 10
        obj_shop.put()
        del_mem_shop(shop_id)

        '''TouchMenu maxtab_touch個作成
        '''
        for idx in range(obj_shop.maxtab_touch):
            obj = TouchMenuTable.insert_or_fail(
                        shop_key    = obj_shop,
                        shop_id     = shop_id,
                        tab_id      = "%02d"%(idx+1),
                        tab_position= idx+1,
                        kname       = u"",
                        kname2      = u"",
                        flag_unlimit = False
                    )


class SelfOrderStaff(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, entity, shops = func_get_common_shopinfo(self, loginData)
        obj = StaffUser.selforder_insert(
                        shop_id  = shop_id,
                        kname    = u"SELF"
                    )

class CourseMenuAdd(Helper):
    def get(self):
        '''CourseMenu 作成
        '''
        loginData = self.logincheck()
        if not loginData: return
        shop_id, entity, shops = func_get_common_shopinfo(self, loginData)
        for idx in range(21,31):
            obj = CourseTable.insert_batch(
                        shop_id   = entity.shop_id,
                        course_cd = "%02d"%idx
                    )



'''
店舗一覧
'''
class PartnerAllShopList(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

        if loginData['shop_user'] .role_flag != 8:
            return
        shop_array = []
        '''
        if obj_shop.company_key.agency_no:
            agency_no = obj_shop.company_key.agency_no
            logging.debug("setup_controller::PartnerAllShopList:: agency_no=%d" %agency_no)
            query = CompanyInfo.all().filter("agency_no=", agency_no)
        else:
            query = CompanyInfo.all().filter("agency_no=", 0)
            logging.debug("setup_controller::PartnerAllShopList:: agency_no not founded")
        '''
        if obj_shop.company_key.agency_no:
            agency_no = obj_shop.company_key.agency_no
        else:
            agency_no = 0
        query = CompanyInfo.all()
        for obj_comp in query:
            if obj_comp.agency_no != agency_no: continue
            shop_query = ShopInfo.all().filter("shop_id > ", obj_comp.company_id ).filter("shop_id < ", obj_comp.company_id + "99")
            #logging.debug("setup_controller::PartnerAllShopList:: shop=%s" %obj_comp.company_id)
            for obj_shop in shop_query:
                shop_array.append(obj_shop)
                #logging.debug("setup_controller::PartnerAllShopList:: shop=%s" %obj_shop.shop_id)

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':lang_sel,
                'control':"システム管理者",
                'title':"店舗リスト",
                'items': shop_array,
            }
        self.helpDispatch("primary", 'setup_allshoplist.html', template_vals)


    def post(self):
        loginData = self.logincheck()
        if not loginData: return

        shop_id = self.request.get('shop_id')

        if shop_id:
            email_id = loginData['shop_user'].email_id
            obj = ShopUser.get_by_key_name(email_id)
            if not obj:
                logging.error('ShopList:: not found user=' + email_id)
                return
            obj.company_key = get_mem_company(shop_id[:6])
            obj.shop_id = shop_id
            obj.put()

            userKey =  "user/" + email_id
            data = memcache.Client().delete( userKey )
            self.redirect("/report/current")
            return


'''
売上修正
'''
class ShopUriageUpdate(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

        #shop_array = []

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':lang_sel,
                'G_LANG_SEL':G_LANG_SEL,
                'control':translate('setup', lang_sel),
                'title':u"売上修正",
                'loginData':loginData,
                'shop_id': shop_id,
                'shops': shops,
                'shop':obj_shop,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
            }
        self.helpDispatch("primary", 'setup_uriage_update.html', template_vals)


    def post(self):
        loginData = self.logincheck()
        if not loginData: return

        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
        start_date, end_date = func_get_fromto_day(self, obj_shop)
        #shop_id = self.request.get('shop_id')
        shop_uriage = int(self.request.get('shop_uriage'))
        shop_group  = int(self.request.get('shop_group'))
        shop_coustomer = int(self.request.get('shop_coustomer'))

        flag = 1
        uriage_down = self.request.get('uriage_down')
        uriage_up   = self.request.get('uriage_up')
        if uriage_down:
            flag = -1
            logging.error('ShopUriageUpdate:: uriage_down checked!!!!')
        if uriage_up:
            logging.error('ShopUriageUpdate:: uriage_up checked!!!!')

        logging.error('ShopUriageUpdate:: start_date={}, uriage={} coustomer={}'.format(start_date, shop_uriage, shop_coustomer))

        obj_day  = DailyUriage.get_or_initialize(
                    shop_id = shop_id,
                    ymd     = start_date
                )
        obj_mon = MonthlyUriage.get_or_initialize(
                    shop_id = shop_id,
                    yyyymm  = start_date[:6]
                )
        if not obj_mon or not obj_day:
            logging.error('ShopUriageUpdate:: not obj_mon or not obj_day' )
            return

        obj_day.uriage += (shop_uriage    * flag)
        obj_day.man    += (shop_coustomer * flag)
        obj_day.count  += (shop_group     * flag)
        #--
        obj_mon.uriage += (shop_uriage    * flag)
        obj_mon.man    += (shop_coustomer * flag)
        obj_mon.count  += (shop_group     * flag)
        #--
        obj_mon.put()
        obj_day.put()
        self.redirect("/setup/uriage_update")
        return



'''
店舗管理者マスタ
'''
class StaffGmailList(Helper):
    def get(self):
        loginData = self.logincheck()
        if not loginData: return
        shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
        #userlist = []
        userlist = ShopUser.all().filter("shop_id =", shop_id)

        lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':G_LANG_SEL,
                'G_LANG_SEL':G_LANG_SEL,
                'control':translate('setup', lang_sel),
                'title':u"アルバイトのアカウント管理",
                'loginData':loginData,
                'shop_id': shop_id,
                'shops': shops,
                'shop':obj_shop,
                'userlist': userlist,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
            }
        self.helpDispatch("primary", 'setup_userlist.html', template_vals)



    def post(self):
        account_id = self.request.get('account_id')
        shop_id    = self.request.get('shop_id')
        role_flag  = 0
        disabl_edit= int(self.request.get('disabl_edit'))

        obj_shop = ShopInfo.get_by_key_name(shop_id)
        if not obj_shop:
            logging.error('ShopUserList:: not found shop_id =%s'%(shop_id))
            return

        flag_tencyo = True
        if role_flag != 1: flag_tencyo = False
        obj_user = ShopUser.get_by_key_name(account_id)
        #
        if self.request.get('search_user'):
            user_id = int(self.request.get('user_id'))
            logging.debug('ShopUserList:: user_id=%s'%(user_id))
            obj_user = ShopUser.get_by_key_name(user_id)
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return
        #
        if self.request.get('update_account'):
            logging.debug('ShopUserList:: update_account=%s'%(account_id))
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return

            obj_user.shop_id     = shop_id
            obj_user.disabl_edit = disabl_edit
            obj_user.regi_no   = 1
            obj_user.role_flag = role_flag
            obj_user.regi_type = 1
            obj_user.group_no  = 0
            obj_user.put()

        if self.request.get('delete_account'):
            flag_tencyo = False
            logging.debug('ShopUserList:: delete_account=%s'%(account_id))
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return
            del_account = self.request.get('del_account')
            if del_account:
                obj_user = ShopUser.get_by_key_name(del_account)
                if obj_user:
                    db.delete( ShopUser.all().filter("email_id =", del_account) )

        if self.request.get('append_account'):
            logging.debug('ShopUserList:: append_account=%s'%(account_id))
            if obj_user:
                logging.error('ShopUserList:: already found user=' + account_id)
                return
            if account_id and shop_id:
                obj_user = ShopUser.insert_or_fail(
                            shop_id     = shop_id,
                            email_id    = account_id,
                            disabl_edit = disabl_edit,
                            regi_no     = 1,
                            role_flag   = role_flag,
                            regi_type   = 1,
                            group_no    = 0
                          )
        if flag_tencyo:
            obj_shop = ShopInfo.get_by_key_name(shop_id)
            if obj_shop:
                obj_shop.admin_id = account_id
                obj_shop.put()
        self.redirect("/setup/staffgmail")
        return


app = webapp.WSGIApplication([
    (slash_dir + "shoplist",   ShopList),
    (slash_dir + "shopedit",   ShopEdit),
    (slash_dir + "staffgmail", StaffGmailList), #2019/5/22 追加予定
    (slash_dir + "uriage_update",  ShopUriageUpdate),
    (slash_dir + "shopseat",   ShopSeat),
    (slash_dir + "seatlayout", ShopSeatLayout),
    #(slash_dir + "shopoption", ShopOption),
    (slash_dir + "csvread",    CsvUploader),
    (slash_dir + "productcsv", ProductCsvUpload),
    (slash_dir + "aspreceipt", AspReceipt),
    (slash_dir + "csvwrite",   CsvDownload),
    (slash_dir + "mastercopy", MasterCopy),
    (slash_dir + "historyclear", HistoryClear),
    (slash_dir + "touchexpand",  TouchMenuTableExpand),
    (slash_dir + "selfstaff",    SelfOrderStaff),
    (slash_dir + "coursemenuadd",   CourseMenuAdd),
    (slash_dir + "partnershoplist", PartnerAllShopList),
    #(slash_dir + "readkenbaiki",    CsvReadKenbaiki),
    ], debug=True)
