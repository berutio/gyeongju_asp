# -*- coding: utf-8 -*-
from google.appengine.api import users
from google.appengine.ext import db, webapp
from google.appengine.ext.webapp import template

from posmodel import *
webapp.template.register_template_library('filters.filter')

import json
import os
import csv
import time
from StringIO import StringIO


dir_name = ""
slash_dirname = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"

def func_invalid_display(self, param):
    template_vals = {
            'title':"エラー内容表示",
            'error':param,
        }
    logging.error('Exception ERROR Message = ' + param)
    self.helpDispatch(dir_name, 'errordisp.html', template_vals)
    return


class MainClass(Helper):
    def get(self):
        try:
            template_vals = {
                    'control':"ORDER",
                    'title':"ORDER",
                }
            self.helpDispatch("primary", 'main.html', template_vals)
        except UserException as e:
            occur_point = "%s::OrderingList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::OrderingList::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise



app = webapp.WSGIApplication([
    (slash_dirname,  MainClass),
    #(slash_dirname + "cart",    OrderConfirm),
    
    ], debug=True)
