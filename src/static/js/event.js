var check_content=0;



$(function(){
	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();

	// start ==== set selectable for class
	$(".category_product").click(function(){// @setupmenu
		$(".category_product").removeClass("category_product_selected");
		$(this).addClass("category_product_selected");
		$("#product_list_div").html("");
		$("#content_id").val($(".category_product_selected > #item_id").val());
		var jsontext =makeJsonData();
		ajaxCall($("#target_name").val(),jsontext,callback);
	});

	$('.product_list').live('click',(function(){// @setupmenu
		$(".product_list").removeClass("product_list_selected");
		$(this).addClass("product_list_selected");
	}));

	$(".touch_product").mousedown(function(){// @setupmenu
		if ($(this).hasClass("touch_product_selected")==false) {
			$(".product_edit").blur();
		}
		$(".touch_product").removeClass("touch_product_selected");
		$(this).addClass("touch_product_selected");
	});

	$(".product_color").click(function(){// @setupmenu
		$(".touch_product_selected").css("background-color",rgb2hex($(this).css("background-color")));
	});

	$('.content_item').live('click',(function(){// @/master/section , @/master/option , @/master/product
		$(".content_item").removeClass("content_item_selected");
		$(".content_item_selected").removeClass("content_item_selected");
		$(this).addClass("content_item_selected");
		$("#content_id").val($(".content_item_selected > #item_id").val());
	}));
	// end ==== set selectable for class


	$("#insertNew").click(function(){
		$("#insert_ajax_div").show();
		wrapWindowByMask();
		$("#mode").val("insert");
	});


	$("#updateContent").click(function(){
		if ($(".content_item_selected").length==0) {
			alert(editNone);
			return false;
		}
		$("#insert_ajax_div").show();

		wrapWindowByMask();
		var i=1;
		$(".inputarea").each(function(){
			//alert("[item_" + i + "] = " +  $(".content_item_selected > #item_"+i).val()); //LSG
			var tmp_value = $(".content_item_selected > #item_"+i).val()
			if( $(this).hasClass("UNDER_DECIMAL") ) {
			    tmp_value = (tmp_value / 100.0).toFixed(2);
			}
			$(this).val(tmp_value);
			i++;
		});
		$("#mode").val("update");
	});

	$("#updatePosition").click(function(){ //  @/master/touchlist
		$("#mode").val("update_position");
		var layoutArray=new Array();
		$(".content_item").each(function(){
			layoutArray.push($(this).children(".listcolumn_big").html());
		});
		$("#position_layout").val(layoutArray);
		//layoutArray
		var arrJson  = new Array();
		arrJson.push("shop_id");
		arrJson.push("mode");
		arrJson.push("target_name");
		arrJson.push("content_id");
		arrJson.push("position_layout");
		var jsontext =string_json(arrJson);
		ajaxCall($("#target_name").val(), jsontext, callback_layout);
	});


	$("#deleteContent").click(function(){ // @/master/section, @/master/option , @/master/product
		if ($(".content_item_selected").length==0) {
			alert(deleteNone);
			return false;
		}
		$("#mode").val("delete");
		if (confirm(checkDelete)) {
			var arrJson  = new Array();
			arrJson.push("shop_id");
			arrJson.push("mode");
			arrJson.push("target_name");
			arrJson.push("content_id");
			arrJson.push("sect_id");
			var jsontext =string_json(arrJson);
			ajaxCall($("#target_name").val(),jsontext,callback);
		}
	});

	$("#ajax_cancel").click(function(){ // @/master/section, @/master/option , @/master/product
		$(".inputarea").val("");
		$(".errordiv").html("");
		$("#insert_ajax_div").hide();
		disableMask();
	});

	$('.product_edit').live('keyup',(function(){// @/master/section , @/master/option , @/master/product
		limitLineTextarea(this,1);
	}));


	$("#ajax_submit").click(function(){ // @/master/section , @/master/option , @/master/product
	  	$(":text,textarea").filter(".validate").each(function(){
			//■必須項目のチェック
			$(this).filter(".required").each(function(){
				if($(this).val()==""){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredInput + "</div>"); //入力必須項目です
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■半角数字チェック（ﾏｲﾅｽ含む）
			$(this).filter(".R_numeric").each(function(){
				if(!$(this).val().match(/^[0-9]+$/g) && !$(this).val().match(/^[-][0-9]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumber + "</div>"); //半角数字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".numeric").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[0-9]*$/g) && !$(this).val().match(/^[-][0-9]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumber + "</div>"); //半角数字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■メールIDチェック
			$(this).filter(".R_mailid").each(function(){
				if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailId + "</div>"); //メールIDの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".mailid").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailId + "</div>"); //メールIDの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■半角英数チェック
			$(this).filter(".R_alpNumeric").each(function(){
				if(!$(this).val().match(/^[a-zA-Z0-9]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".alpNumeric").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[a-zA-Z0-9]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■浮動小数チェック
			$(this).filter(".R_float").each(function(){
				if(!$(this).val().match(/^[0-9]+\.?[0-9]*$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumberDot + "</div>"); //半角数字のみで入力してください（小数点含む）
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■浮動小数チェック
			$(this).filter(".float").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[0-9]+\.?[0-9]*$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumberDot + "</div>"); //半角数字のみで入力してください（小数点含む）
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
		});

		//■■SELECTのチェック
	    $("option:selected").parent().filter(".validate").each(function(){
			$(this).filter(".required").each(function(){
				if($(this).val()=="0"){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredTerm + "</div>");  //入力必須項目です。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
		});


		/* ■エラーが表示されていたら送信しない*/
		if($('div').hasClass('errorMsg') == true){
			alert( inputdataError ); //入力データにエラーが存在します。
			return false;
		}
/*
		if(confirm( sendInputdata )){ //以上の内容で送信します。宜しいですか？
			var jsontext = makeJsonData();
			ajaxCall($("#target_name").val(), jsontext, callback);
		} else {
			return false;
		}
*/
		var jsontext = makeJsonData();
		ajaxCall($("#target_name").val(), jsontext, callback);
	});

	//=============FORM===========================
	$(".date_form").each(function(){
		$(this).html($(this).html().substr(0,4)+"年"+$(this).html().substr(4,2)+"月"+$(this).html().substr(6,2)+"日");
	});

	$(".time_form").each(function(){
		if ($(this).html() == "0") {
			$(this).html("");
		} else {
			$(this).html("000" + $(this).html());
			$(this).html($(this).html().substr(-4,2)+":"+$(this).html().substr(-2,2));
		}
	});

	$(".time_form3").each(function(){
		if ($(this).html() == "0") {
			$(this).html("");
		} else {
			$(this).html("     " + $(this).html());
			$(this).html($(this).html().substr(-6,4)+":"+$(this).html().substr(-2,2));
		}
	});

	$(".money_form").each(function(){
		if (this.tagName=="INPUT") {
			$(this).val(number_format($(this).val(),0));
		}else {
			$(this).html(number_format($(this).html(),1));
		}
   	});

	$(".money_form_blank").each(function(){
		$(this).html(number_format($(this).html(),4));
   	});

	$(".minus_money_form").each(function(){
		if (this.tagName=="INPUT") {
			$(this).val(number_format($(this).val(),0));
		}else {
			$(this).html(number_format($(this).html(),1));
		}
   	});

	$(".number_form").each(function(){
		if (this.tagName=="INPUT") {
			$(this).val(number_format($(this).val(),0));
		}else {
			$(this).html(number_format($(this).html(),0));
		}
   	});

	$(".number_form_blank").each(function(){
		$(this).html(number_format($(this).html(),5));
   	});

	$(".percent_form").each(function(){
		$(this).html(number_format($(this).html(),2));
   	});

	//---------------------------------------------------------------
	// 入力フォームの上にテキストを表示する「プレースホルダ（placeholder）
	//---------------------------------------------------------------
    $('input[type=text],input[type=password],textarea').each(function(){
        var thisTitle = $(this).attr('title');
        if(!(thisTitle === '' || thisTitle == undefined )){
            $(this).wrapAll('<span style="text-align:left;display:inline-block;position:relative;"></span>');
            $(this).parent('span').append('<span class="placeholder">' + thisTitle + '</span>');
            $('.placeholder').css({
                top:'4px',
                left:'5px',
                fontSize:'100%',
                lineHeight:'120%',
                textAlign:'left',
                color:'#999',
                overflow:'hidden',
                position:'absolute',
                zIndex:'99'
            }).click(function(){
                $(this).prev().focus();
            });

            $(this).focus(function(){
                $(this).next('span').css({display:'none'});
            });

            $(this).blur(function(){
                var thisVal = $(this).val();
                if(thisVal === ''){
                    $(this).next('span').css({display:'inline-block'});
                } else {
                    $(this).next('span').css({display:'none'});
                }
            });

            var thisVal = $(this).val();
            if(thisVal === ''){
                $(this).next('span').css({display:'inline-block'});
            } else {
                $(this).next('span').css({display:'none'});
            }
        }
    });
});


$(function(){
	$(".listcontent:odd").css("background-color", "#f5f3d6");
});

function select_content(num){
	$('.listcontent').css('border','solid 1px #ffffff');
	$('.listcontent').css('background-color','#ffffff');

	$(".listcontent:odd").css("background-color", "#f5f3d6");
	//$(".listcontent:odd").css("background-color", "#d7d28e");
	/*
	$('#'+ num +'_tr').css('border','solid 1px #cc5c00');
	$('#'+ num +'_tr').css('background-color','#ff9842');
	*/
	$('#'+ num +'_tr').css('border','solid 1px #cc5c00');
	$('#'+ num +'_tr').css('background-color','#adff2f');
	check_content=num;
	//alert("num=" + num + ", check_content=" + check_content);
}

function select_content_B(num){
	$('.listcontent').css('border','solid 1px #ffffff');
	$('.listcontent').css('background-color','#ffffff');

	$(".listcontent:odd").css("background-color", "#f5f3d6");
	$('#'+ num +'_tr').css('border','solid 1px #cc5c00');
	$('#'+ num +'_tr').css('background-color','#adff2f');
	$('#'+ num +'B_tr').css('border','solid 1px #cc5c00');
	$('#'+ num +'B_tr').css('background-color','#adff2f');
	check_content=num;
}

function editcontent(url, selid){
//	alert("url=" + url + ", param=" + selid);
	if(selid==null || selid==0){
		alert(unchecked);
		return false;
	}else{
		move(url+selid);
	}
}

function deletecontent(url, selid){
	if(selid==null || selid==0){
		alert(unchecked);
		return false;
	}else{
		if (confirm(deletecheck)) {
			move(url+selid+"&delete=delete");
		}
	}
}


function move(url){
	document.location.href=url;
}

//
function CsvDownloadDisable(){
	$("#csvdownload").attr('disabled', true);
	$("#csvdownload").removeClass("btn_blue_small").addClass("btn_small_unuse");
}

//
function CsvDownloadDisableMid(){
	$("#csvdownload").attr('disabled', true);
	$("#csvdownload").removeClass("btn_blue_mid").addClass("btn_mid_unuse");
}
