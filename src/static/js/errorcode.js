var unchecked = "対象が選択されていません。";
var deletecheck = "削除してもよろしいですか？";
var updatecheck = "修正してもよろしいですか？";
var noresult = "検索結果がありません。";
var unselected_group = "グループを選択して下さい。";
var unselected_store = "店舗を選択して下さい。";
var short_insert="入力したコメントがありません。";
//pos error
var shukkin="既に出勤が登録されている為、登録できません。";
var motaikin="既に退勤が登録されている為、登録できません。";
var madashukkin="出勤記録が無い為、登録できません。まず、出勤を登録して下さい。";
var overrest="休憩登録は既に２回が登録されてる為、登録できません。";
var beforeout = "既に休憩INが登録されてる為、登録出来ません。";
var madarest="休憩INの記録が無い為、登録出来ません。";

var dateover="検索期間は1年まで可能です。";

var inputerror="入力方法を確認して下さい。";

var unselected_content = "対象を選択して下さい。";

var input_receipt="入力数の桁数が違います。";

var input_hitsuyo="トタール購入額、来店回数は必須項目です。";

var check_end_date="開始日と終了日を確認して下さい。";

var over_selected_menu="メニューは最大２０個まで選択出来ます。";

var no_result_customer="該当データなし";

var no_name="氏名を入力してください。";

var no_condition="検索条件を入力して下さい。";

