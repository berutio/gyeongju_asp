(function($) {
	var T = {};
	T.$this = null;
	T.OPTION = {
		rows: [],
		sum_array: [],
		time_table_values: {
			time_start    : 17,
			time_end      : 24,
			time_interval : 15, // 0, 15, 30,
			time_span     : 4,
			time_unit_cols: 0,
		},
		size: {
			time_unit_width    : 30,
			time_unit_height   : 30,
			fixed_margin_bottom: 0,
			fixed_margin_right : 0,
			fixed_total_height : 0,
			fixed_total_width  : 0,
			sched_min_height   : 150,
		},
		scroll : {
			hor_max           : 0,
			hor_base_diff     : -1500,
			hor_base_interval : 2000,
			hor_interval      : 0,
			hor_diff_drag     : 30,
			hscroll_max       : 0,
			vscroll_max       : 0,
			vert_max          : 0,
			vert_diff         : 15,
			vert_diff_drag    : 30,
		},
		scrollInterval: {
			$up    : null,
			$down  : null,
			$left  : null,
			$right : null
		},
		dragOffset: {
			$top   : 0,
			$left  : 0
		},
		dragScrollSpeedSetting: 250,
		current_date: null,
		onPrereserve: function(data) {},
		onCancelReserve: function(data) {},
		onUpdateReserve: function(changedData, target, success, fail) {
			//
		},
		onClickReserve: function(data) {
			console.log("## onClickReserve ##");
			console.log(data);
			$('#reserve_id').val(data.reserve_id);
			$('#date_time').html(data.start_date + "<br>滞在時間：" + data.staytime + "分");
			$('#telno').val(data.telno);
			$('#kname').val(data.kname);
			$('#persons').val(data.person);
			$('#mail_ad').val(data.mail_ad);
			$('#comment').val(data.comment);

			T.part.$dialog.dialog( "open" );
			T.status.event_flg = 'nodrag';
		},
		onDbClickReserve: function(data) {},
	};
	T.status = {
		b_touch : false,
		seat_count: {
			prereserve: 0,
			reserve: 0
		},
		select_action_holder : {
			b_move: false
		},
		dragging: false,
		event_flg: 'nodrag',
		reserveMoveStartTop    : 0,
		reserveMoveStartLeft   : 0,
		reserveMoveStartHeight : 0,
		reserveMoveStartWidth  : 0,
		reserveMoveStartNameWidth: 0,
		displayFlg: true,
		clicks: 0,
		timer: null,
	};
	T.data = {
		seats: {
			prereserve: [],
			reserve: []
		},
		current_date : null,
		reserve_list: [],
	};
	T.part = {
		 $holder              : null
		,$hs_wrapper          : null
		,$hs                  : null
		,$btn_hs_prev         : null
		,$btn_hs_next         : null
		,$vs_header           : null
		,$vs_wrapper          : null
		,$vs                  : null
		,$vs_move             : null
		,$vs_linkage_wrapper  : null
		,$vs_linkage          : null
		,$vs_linkage_inner    : null
		,$vs_sbar_wrapper     : null
		,$vs_sbar             : null
		,$data_item_move_box  : null
		,$vert_band           : null
		,$prereserve_box      : null
		,$reserve_box         : null
		,$refuse_box          : null
		,$dialog              : null
	};

	T.preReserveData = {
		 start_date : ""
		,staytime   : ""
		,seats  : []
		,kname  : ""
		,telno  : ""
		,mail_ad: ""
		,comment: ""
		,person: 0
	};


	T.fn = {};

	T.fn.initSettings = function() {
		var start = T.OPTION.time_table_values.time_start;
		var end = T.OPTION.time_table_values.time_end;
		var interval = T.OPTION.time_table_values.time_interval;
		var span = 1;
		var width = 120;
		if (interval == 15) {
			span = 4;
			width = 30;
		} else if (interval == 30) {
			span = 2;
			width = 60;
		}
		var cols = (end - start) * span;
		T.OPTION.time_table_values.time_span = span;
		T.OPTION.time_table_values.time_unit_cols = cols;
		T.OPTION.size.time_unit_width = width;
		T.OPTION.scroll.vert_diff = T.OPTION.size.time_unit_height / 2;
		T.OPTION.scroll.vert_diff_drag = T.OPTION.size.time_unit_height;
	};

	T.fn.makeTable = function() {
		T.$this.empty();

		var sched = $('<div id="sched">');
		var inner = $('<div id="inner">');
		T.$this.append(sched);
		sched.append(inner);
		inner.append('<div id="seat_list_wrapper" class="float-L">');
		inner.append('<div id="tt_hs_prev" class="float-L">' +
				'<a href="#">' +
				'<img src="/static/timetable-img/btn_prevtime.gif" alt="前へ" width="21" height="21" class="main">' +
				'</a></div>');
		inner.append('<div id="tt_wrapper" class="float-L hs">');

		$("#seat_list_wrapper").append(T.fn.makeTableInfo());
		$("#seat_list_wrapper").append(T.fn.makeSeatList(T.OPTION.rows));

		var tt = $('<div id="tt" class="hs_inner" style="">');
		$("#tt_wrapper").append(tt);
		$("#tt").append(T.fn.makeTimetableHeader(T.OPTION.time_table_values, T.OPTION.size, T.OPTION.sum_array));
		$("#tt").append(T.fn.makeTimetableBody(T.OPTION.time_table_values, T.OPTION.size, T.data.seats.reserve));
		inner.append('<div id="tt_sbar" class="float-L">' +
				'<div id="tt_sbar_base">' +
				'<div id="tt_sbar_move" class="ui-draggable">' +
				'<div class="upper"></div>' +
				'<div class="center"></div>' +
				'<div class="lower"></div>' +
				'</div>' +
				'</div>' +
				'</div>');
		inner.append('<div id="tt_hs_next" class="float-L">' +
				'<a href="#">' +
				'<img src="/static/timetable-img/btn_nexttime.gif" alt="次へ" width="21" height="21" class="main">' +
				'</a></div>');
		inner.append('<div id="inner_end" class="clear"><hr></div>');
		T.fn.setObj();
	};

	T.fn.setObj = function() {
		T.part.$holder             = $('#sched');
		T.part.$hs_wrapper         = $('.hs', T.part.$holder);
		T.part.$hs                 = $('.hs_inner', T.part.$holder);
		T.part.$btn_hs_prev        = $('#tt_hs_prev a');
		T.part.$btn_hs_next        = $('#tt_hs_next a');
		T.part.$vs_linkage_wrapper = $('#seat_list_wrapper');
		T.part.$vs_linkage         = $('#seat_list');
		T.part.$vs_linkage_inner   = $('#seat_list_inner');
		T.part.$vs_header          = $('#tt_header');
		T.part.$vs_wrapper         = $('#tt_body');
		T.part.$vs                 = $('#tt_body_inner');
		T.part.$vs_move            = $('#sched .vs_inner');
		T.part.$data_item_move_box = $('#data_item_move_box');
		T.part.$vs_sbar_wrapper    = $('#tt_sbar_base');
		T.part.$vs_sbar            = $('#tt_sbar_move');
	}

	T.fn.makeTableInfo = function() {
		var div ='<div id="tableInfo" style="font-size:13px;">'
					+  '<p>予約時間</p>'
					+  '<p>分</p>'
					+  '<p>予約人数</p>'
				+ '</div>';
		return div;
	}
	T.fn.makeSeatList = function(rows) {
		var seatList = $('<div id="seat_list" class="vs">');
		var seatListInner = $('<div id="seat_list_inner" class="vs_inner">');
		seatList.append(seatListInner);
		seatListInner.append(T.fn.makeSeatListTable("prereserve", rows));
		seatListInner.append(T.fn.makeSeatListTable("reserve", rows));
		return seatList;
	}

	T.fn.makeSeatListTable = function(className, rows) {
		var table = $('<table>');
		table.addClass(className);
		var tbody = $('<tbody>');
		table.append(tbody);

		var rowCount = rows.length;
		var seatClassName = "seat";

		if (className == "prereserve") {
			seatClassName = "seatx";
			rowCount = 1;
		}

		for (var i= 0; i< rowCount; i++) {
			var tr = $('<tr>');
			tbody.append(tr);
			var td1 = "";
			if (className == "prereserve") {
				td1 = $('<td colspan="2" style="text-align: center;">');
			} else {
				td1 = $('<td>');
			}
			td1.addClass(seatClassName + (i+1));
			td1.addClass("name");
			if (className == "prereserve") {
				td1.addClass("bbottom");
				td1.text("座席指定なし");
				td1.append(span);
			} else {
				var span = $('<span>');
				span.text(rows[i]["seatName"]);
				td1.append(span);
			}
			tr.append(td1);


			if (className != "prereserve") {
				var td2 = $('<td>');
				td2.addClass(seatClassName + (i+1));
				td2.addClass("num");
				if (className == "prereserve") {
					td2.addClass("bbottom");
					td2.text("なし");
				} else {
					td2.text(rows[i]["seatDesc"]);
				}
				tr.append(td2);
			}
			if (className == "prereserve") {
				T.status.seat_count.prereserve += 1;
			} else {
				T.status.seat_count.reserve += 1;
			}
		}

		return table;
	}

	T.fn.makeTimetableHeader = function(settings, size, sum_array) {
		var header = $('<div id="tt_header">');
		var table = $('<table colspacing="0" colpadding="0" padding="0">');
		var tbody = $('<tbody>');
		var tr_hour = $('<tr class="hour">');
		var tr_min = $('<tr class="min">');
		var tr_empty = $('<tr class="empty">');
		var tr_manct = $('<tr class="mancounter">');
		header.append(table);
		table.append(tbody);
		tbody.append(tr_hour);
		tbody.append(tr_min);
		tbody.append(tr_empty);
		tbody.append(tr_manct);

		var k = 0;
		for (var i=settings.time_start, k=0; i<settings.time_end; i++, k++) {
			for (var j=0; j<settings.time_span; j++) {
				var th = $('<th>' + sum_array[k][j] + '</th>');
				var className = "";
				if (j == 0)
					th.addClass("oclock");
				if ((i+1) == settings.time_end && (j+1) == settings.time_span)
					th.addClass("last");
				tr_manct.append(th);
			}
		}

		for (var i=settings.time_start; i<settings.time_end; i++) {
			tr_hour.append('<th colspan="' + settings.time_span + '">' + i + '時</th>');
			for (var j=0; j<settings.time_span; j++) {
				var th = $('<th>' + (j*settings.time_interval) + '</th>');
				var className = "";
				if (j == 0)
					th.addClass("oclock");
				if ((i+1) == settings.time_end && (j+1) == settings.time_span)
					th.addClass("last");
				tr_min.append(th);
				var emptyTh = th.clone();
				emptyTh.text("");
				tr_empty.append(emptyTh);
			}
		}
		table.width(size.time_unit_width * settings.time_unit_cols + 1);
		return header;
	};

	T.fn.makeTimetableBody = function(settings, size, rows) {
		var body = $('<div id="tt_body" class="vs">');
		var bodyInner = $('<div id="tt_body_inner" class="vs_inner">');
		body.append(bodyInner);
		bodyInner.append(T.fn.makeTimetableBodyTable(settings, size, "prereserve", 1));
		bodyInner.append(T.fn.makeTimetableBodyTable(settings, size, "reserve", rows.length));
		return body;
	}

	T.fn.makeTimetableBodyTable = function(settings, size, className, rowCount) {
		var table = $('<table colspacing="0" colpadding="0" padding="0">');
		table.addClass(className);
		var tbody = $('<tbody>');
		table.append(tbody);

		var seatClassName = "seat";
		if (className == "prereserve")
			seatClassName = "seatx";

		var convertZero = function(num) {
			if (num < 10)
				return "0" + num;
			return num;
		};

		for (var i= 0; i< rowCount; i++) {
			var tr = $('<tr>');
			tbody.append(tr);
			for (var j=settings.time_start; j<settings.time_end; j++) {
				for (var k=0; k<settings.time_span; k++) {
					var td = $('<td>');
					td.addClass(seatClassName + (i+1));
					td.addClass("hour" + convertZero(j) + convertZero(k*settings.time_interval));
					if (k == 0) {
						td.addClass("oclock");
					}
					if ((j+1) == settings.time_end && (k+1) == settings.time_span) {
						td.addClass("last");
					}
					td.addClass("time");
					tr.append(td);
				}
			}
		}
		table.width(size.time_unit_width * settings.time_unit_cols + 1);
		return table;
	}

	T.fn.makeReserveInfoDialog = function() {
		var dialogWidget = $('<div id="dialog-form" title="予約情報">'+
				'<fieldset><table>'+
					'<tr>'+
				      '<td>予約日時　</td>'+
				      '<td><span id="date_time"></span>' +
				          '<input type="hidden" name="reserve_id" id="reserve_id" value="">' +
				      '</td>'+
					'</tr><tr>'+
				      '<td>電話番号</td>'+
				      '<td><input type="text" name="telno" id="telno" value="" class="text ui-widget-content ui-corner-all" placeholder="必須項目"></td>'+
					'</tr><tr>'+
				      '<td>名前</td>'+
				      '<td><input type="text" name="kname" id="kname" value="" class="text ui-widget-content ui-corner-all" placeholder="必須項目"></td>'+
					'</tr><tr>'+
				      '<td>人数</td>'+
				      '<td><input type="text" name="persons" id="persons" value="" class="text ui-widget-content" placeholder="必須項目"></td>'+
					'</tr><tr>'+
				      '<td>E-Mail</td>'+
				      '<td><input type="text" name="mail_ad" id="mail_ad" value="" class="text ui-widget-content ui-corner-all"></td>'+
					'</tr><tr>'+
				      '<td>コメント</td>'+
				      '<td><input type="text" name="comment" id="comment" value="" class="text ui-widget-content ui-corner-all"></td>'+
					'</tr><tr>'+
				'</table></fieldset>'+
				'</div>');
		T.part.$dialog = dialogWidget.dialog({
		    autoOpen: false,
		    height: 470,
		    width: 350,
		    modal: true,
		    draggable: true,
		    resizable: false,
		    buttons: [
		    	{
					text: "予約取消",
					id: "dialog_cancel",
					click: function() {
						console.log("Create an Reservation");
						T.preReserveData.reserve_id = $("#reserve_id").val();
						//
						T.OPTION.onCancelReserve(T.preReserveData);
						T.part.$dialog.dialog( "close" );
						$('.pre_reserve').remove();
					}
		    	},
		    	{
					text: "予約",
					id: "dialog_ok",
					click: function() {
						console.log("Create an Reservation");
						var kname   = $("#kname").val();
						var telno   = $("#telno").val();
						var persons = $("#persons").val();
						if( kname && telno && persons) {
							T.preReserveData.reserve_id = $("#reserve_id").val();
							T.preReserveData.kname   = kname;
							T.preReserveData.telno   = telno;
							T.preReserveData.person  = persons;
							T.preReserveData.mail_ad = $("#mail_ad").val();
							T.preReserveData.comment = $("#comment").val();
							//
							T.OPTION.onPrereserve(T.preReserveData);
							T.part.$dialog.dialog( "close" );
							$('.pre_reserve').remove();
						} else {
							alert('必須入力項目を全て入力して下さい。');
						}
					}
		    	},
		    	{
					text: "Close",
					id: "dialog_close",
					click: function() {
			        	T.part.$dialog.dialog( "close" );
						$('.pre_reserve').remove();
			        }
		    	}
		    ],
		    open: function(event, ui) {
		    	console.log("#### open ####");
		    	//console.log(event.target);
		    },
		    close: function() {
		    }
		});
	};


	T.fn.initSize = function() {
		T.OPTION.size.fixed_total_height = T.$this.position().top +
			(T.part.$holder.outerHeight() - T.part.$holder.height()) +
			T.fn.utils.convertInt($("body").css("margin-bottom")) +
			T.fn.utils.convertInt($("body").css("padding-bottom")) +
			$("#inner_end hr").outerHeight(true) +
			T.OPTION.size.fixed_margin_bottom;
		T.OPTION.size.fixed_total_width = T.$this.position().left +
			T.part.$vs_linkage_wrapper.outerWidth(true) +
			T.part.$btn_hs_prev.parent().outerWidth(true) +
			T.part.$btn_hs_next.parent().outerWidth(true) +
			T.part.$vs_sbar_wrapper.parent().outerWidth(true) +
			T.fn.utils.convertInt($("body").css("margin-right")) +
			T.fn.utils.convertInt($("body").css("padding-right")) +
			T.OPTION.size.fixed_margin_right;
		T.fn.setSize();
	}


	T.fn.setSize = function() {
		var sched_height = $(window).outerHeight(true) - T.OPTION.size.fixed_total_height;

		if (sched_height < (T.part.$vs_header.height() + T.OPTION.size.sched_min_height)) {
			sched_height = T.part.$vs_header.height() + T.OPTION.size.sched_min_height;
		}
		var sched_height_max = T.part.$vs_header.height() + T.part.$vs.height();
		if (sched_height > sched_height_max)
			sched_height = sched_height_max;
		var sched_inner_height = sched_height - T.part.$vs_header.height();
		if (sched_inner_height > T.part.$vs.height())
			sched_inner_height = T.part.$vs.height();

		T.part.$hs_wrapper.height(sched_height);
		T.part.$btn_hs_prev.height(sched_height);
		T.part.$btn_hs_next.height(sched_height);
		T.part.$vs_linkage.height(sched_inner_height);
		T.part.$vs_wrapper.height(sched_inner_height);
		T.part.$vs_sbar_wrapper.height(sched_inner_height);
		$("#tt_sbar").css("padding-top", sched_height - sched_inner_height);

		var move_top = sched_inner_height - T.part.$vs_move.height();
		if (move_top > T.part.$vs_move.position().top) {
			T.part.$vs_move.css('top', move_top + 'px');
		}

		var sched_width = $(window).width() - T.OPTION.size.fixed_total_width;
		if (sched_width < T.OPTION.size.sched_min_width)
			sched_width = T.OPTION.size.sched_min_width;
		if (sched_width > T.part.$hs.width())
			sched_width = T.part.$hs.width();

		T.part.$hs_wrapper.width(sched_width);
		var move_left = sched_width - T.part.$hs.width();
		if (move_left > T.part.$hs.position().left) {
			T.part.$hs.css('left', move_left + 'px');
		}
		T.fn.setScroller();
	}


	T.fn.setScroller = function() {
		T.OPTION.scroll.hor_max = T.part.$hs_wrapper.width() - T.part.$hs.width();
		T.OPTION.scroll.hor_interval =
			(1 - (T.OPTION.scroll.hor_base_diff - T.OPTION.scroll.hor_max) / T.OPTION.scroll.hor_base_diff) *
			T.OPTION.scroll.hor_base_interval;
		T.OPTION.scroll.vert_max   = T.part.$vs_wrapper.height() - T.part.$vs.height();

		var hs_sbar_rate   = T.part.$hs_wrapper.width() / T.part.$hs.width();
		var hs_sbar_width  = Math.floor(hs_sbar_rate * T.part.$hs_wrapper.width());
		T.OPTION.scroll.hscroll_max = T.part.$hs_wrapper.width() - hs_sbar_width;

		if (hs_sbar_rate >= 1) {
			T.OPTION.scroll.b_hor_scroll = false;
			T.fn.clearEventScheduleHorizontalScroll();
		} else {
			T.OPTION.scroll.b_hor_scroll = true;
			T.fn.setEventScheduleHorizontalScroll();
		}

		var vs_sbar_rate   = T.part.$vs_wrapper.height() / T.part.$vs.height();
		var vs_sbar_height = Math.floor(vs_sbar_rate * T.part.$vs_wrapper.height());
		T.OPTION.scroll.vscroll_max = T.part.$vs_wrapper.height() - vs_sbar_height;

		T.part.$vs_sbar.draggable({
			 axis: 'y'
			,containment: 'parent'
			,drag: T.fn.move_drag_sbar
			,start: function() {

			}
			,stop: function() {

			}
		});

		if (vs_sbar_rate >= 1) {
			T.part.$vs_sbar.hide();
			T.OPTION.scroll.b_vert_scroll = false;
			T.fn.clearEventScheduleVerticalScroll();
		} else {
			T.part.$vs_sbar.height(vs_sbar_height);
			var bar_top = T.part.$vs_wrapper.height() - vs_sbar_height;
			if (bar_top < T.part.$vs_sbar.position().top) {
				T.part.$vs_sbar.css('top', bar_top + 'px');
			}
			T.part.$vs_sbar.show();
			T.data.b_vert_scroll = true;
			T.fn.setEventScheduleVerticalScroll();
		}
	};


	T.fn.setEventScheduleHorizontalScroll = function() {
		if (T.status.b_touch) {
			T.part.$btn_hs_prev.bind('touchstart', T.fn.start_sched_prev);
			T.part.$btn_hs_prev.bind('touchend',   T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.bind('click',      function(){return false;});
			//
			T.part.$btn_hs_next.bind('touchstart', T.fn.start_sched_next);
			T.part.$btn_hs_next.bind('touchend',   T.fn.stop_sched_hor);
			T.part.$btn_hs_next.bind('click',      function(){return false;});
		} else {
			T.part.$btn_hs_prev.bind('mousedown',  T.fn.start_sched_prev);
			T.part.$btn_hs_prev.bind('mouseup',    T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.bind('mouseout',   T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.bind('click',      function(){return false;});
			//
			T.part.$btn_hs_next.bind('mousedown',  T.fn.start_sched_next);
			T.part.$btn_hs_next.bind('mouseup',    T.fn.stop_sched_hor);
			T.part.$btn_hs_next.bind('mouseout',   T.fn.stop_sched_hor);
			T.part.$btn_hs_next.bind('click',      function(){return false;});

		}
	};


	T.fn.clearEventScheduleHorizontalScroll = function() {
		if (T.status.b_touch) {
			T.part.$btn_hs_prev.unbind('touchstart', T.fn.start_sched_prev);
			T.part.$btn_hs_prev.unbind('touchend',   T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.unbind('click',      function(){return false;});
			//
			T.part.$btn_hs_next.unbind('touchstart', T.fn.start_sched_next);
			T.part.$btn_hs_next.unbind('touchend',   T.fn.stop_sched_hor);
			T.part.$btn_hs_next.unbind('click',      function(){return false;});
		} else {
			T.part.$btn_hs_prev.unbind('mousedown', T.fn.start_sched_prev);
			T.part.$btn_hs_prev.unbind('mouseup',   T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.unbind('mouseout',  T.fn.stop_sched_hor);
			T.part.$btn_hs_prev.unbind('click',     function(){return false;});
			//
			T.part.$btn_hs_next.unbind('mousedown', T.fn.start_sched_next);
			T.part.$btn_hs_next.unbind('mouseup',   T.fn.stop_sched_hor);
			T.part.$btn_hs_next.unbind('mouseout',  T.fn.stop_sched_hor);
			T.part.$btn_hs_next.unbind('click',     function(){return false;});
		}
	};


	T.fn.start_sched_prev = function(event) {
		event.preventDefault();
		T.fn.scroll_sched_hor_prev();
	};


	T.fn.start_sched_next = function(event) {
		event.preventDefault();
		T.fn.scroll_sched_hor_next();
	};


	T.fn.scroll_sched_hor_prev = function() {
		var pos  = T.part.$hs.position();
		var diff = 1 - (T.OPTION.scroll.hor_max - pos.left) / T.OPTION.scroll.hor_max;
		if (pos.left < 0)
			T.part.$hs.stop().animate({left : 0}, diff * T.OPTION.scroll.hor_interval, 'linear');
	};


	T.fn.scroll_sched_hor_next = function() {
		var pos  = T.part.$hs.position();
		var diff = 1 - pos.left / T.OPTION.scroll.hor_max;
		if (pos.left > T.OPTION.scroll.hor_max)
			T.part.$hs.stop().animate({left: T.OPTION.scroll.hor_max + 'px'}, diff * T.OPTION.scroll.hor_interval, 'linear');
	};


	T.fn.scroll_sched_hor_prev_drag = function(ghost) {
		var pos = T.part.$hs.position();
		if (pos.left < 0) {
			T.part.$hs.css('left', (pos.left + T.OPTION.scroll.hor_diff_drag) + 'px');
		}
	};


	T.fn.scroll_sched_hor_next_drag = function(ghost) {
		var pos  = T.part.$hs.position();

		if (pos.left > T.OPTION.scroll.hor_max) {
			T.part.$hs.css('left', (pos.left - T.OPTION.scroll.hor_diff_drag) + 'px');
		}
	};


	T.fn.stop_sched_hor = function(event) {
		event.stopPropagation();

		T.part.$hs.stop();
		return false;
	};


	T.fn.move_drag_sbar = function(event, obj) {
		var move_rate = obj.position.top / T.OPTION.scroll.vscroll_max;
		var top = T.OPTION.scroll.vert_max * move_rate;
		T.part.$vs_move.css('top', top + 'px');
	};


	T.fn.setEventScheduleVerticalScroll = function() {
		T.part.$vs_wrapper.bind('mouseover', T.fn.start_mousewheel);
		T.part.$vs_wrapper.bind('mouseout', T.fn.stop_mousewheel);
		T.part.$vs_linkage_wrapper.bind('mouseover', T.fn.start_mousewheel);
		T.part.$vs_linkage_wrapper.bind('mouseout', T.fn.stop_mousewheel);
	};


	T.fn.clearEventScheduleVerticalScroll = function() {
		T.part.$vs_wrapper.unbind('mouseover', T.fn.start_mousewheel);
		T.part.$vs_wrapper.unbind('mouseout', T.fn.stop_mousewheel);
		T.part.$vs_linkage_wrapper.unbind('mouseover', T.fn.start_mousewheel);
		T.part.$vs_linkage_wrapper.unbind('mouseout', T.fn.stop_mousewheel);
		T.fn.stop_mousewheel();
	};


	T.fn.start_mousewheel = function() {
		T.fn.stop_mousewheel();

		T.part.$vs_wrapper.bind('mousewheel', T.fn.scroll_sched_vert);
		T.part.$vs_linkage_wrapper.bind('mousewheel', T.fn.scroll_sched_vert);
		T.part.$vs_sbar_wrapper.bind('mousewheel', T.fn.scroll_sched_vert);
	};

	T.fn.stop_mousewheel = function() {
		T.part.$vs_wrapper.unbind('mousewheel', T.fn.scroll_sched_vert);
		T.part.$vs_linkage_wrapper.unbind('mousewheel', T.fn.scroll_sched_vert);
		T.part.$vs_sbar_wrapper.unbind('mousewheel', T.fn.scroll_sched_vert);
	};


	T.fn.scroll_sched_vert = function(event, delta) {
		delta = parseInt(event.originalEvent.wheelDelta, 10);

		if (delta < 0) {
			var pos = T.part.$vs_move.position();
			var top = pos.top - T.OPTION.scroll.vert_diff;
			if (top < T.OPTION.scroll.vert_max) {
				top = T.OPTION.scroll.vert_max;
			}

			T.part.$vs_move.css('top', top + 'px');

			var scroll_move_rate = top / T.OPTION.scroll.vert_max;
			if (scroll_move_rate > 1)
				scroll_move_rate = 1;
			T.part.$vs_sbar.css('top', Math.floor(scroll_move_rate * T.OPTION.scroll.vscroll_max) + 'px');
		} else if (delta > 0) {
			var pos = T.part.$vs_move.position();
			var top = pos.top + T.OPTION.scroll.vert_diff;
			if (top > 0) {
				top = 0;
			}
			T.part.$vs_move.css('top', top + 'px');

			var scroll_move_rate = top / T.OPTION.scroll.vert_max;
			if (scroll_move_rate > 1)
				scroll_move_rate = 1;
			T.part.$vs_sbar.css('top', Math.floor(scroll_move_rate * T.OPTION.scroll.vscroll_max) + 'px');

		}
		return false;
	};


	T.fn.utils = {};

	T.fn.utils.convertInt = function(str) {
		var value = parseInt(str);
		if (value == NaN)
			return 0;
		return value;
	}


	T.fn.setActions = function() {
		$(document).on('mouseenter', 'td.time', T.fn.timeTableHoverOn).on('mouseleave', 'td.time', T.fn.timeTableHoverOff);

//		$(document).on('click', 'td.time', T.fn.select_mode_timeTable);
		$(document).on('mousedown touchstart', '.time', T.fn.select_start_TimeTable);
		$(document).on('mousemove touchmove', '.time', T.fn.select_move_TimeTable);
		$(document).on('mouseup touchend', '.time, .pre_reserve', T.fn.select_end_TimeTable);
		$(document).on('click', 'div.pre_reserve .multi_select', function(){T.fn.pre_reserve_band($(this))});
		if (T.status.b_touch) {
			$(document).on("click", "div.reserve", T.fn.reserve_band_single).on("dblTap", "div.reserve", T.fn.reserve_band_double);
		} else {
			$(document).on("click", "div.reserve", T.fn.reserve_band_single).on("dblclick", "div.reserve", T.fn.reserve_band_double);
		}
	}


	T.fn.timeTableHoverOn = function(event) {
		var vclass = $(this).attr('class');
		var svclass = vclass.split(/[\s]/);
		$('td.'+svclass[0]).addClass('cselect');
		$('td.'+svclass[1]).addClass('cselect');
	}
	T.fn.timeTableHoverOff = function(event) {
		var vclass = $(this).attr('class');
		var svclass = vclass.split(/[\s]/);
		$('td.'+svclass[0]).removeClass('cselect');
		$('td.'+svclass[1]).removeClass('cselect');
	}


	T.fn.get_select_pos_TimeTable = function(event) {
		var x = 0;
		var y = 0;
		if (T.status.b_touch) {
			if (event.originalEvent.touches.length > 0) {
				x = event.originalEvent.touches[0].pageX;
				y = event.originalEvent.touches[0].pageY;
			}
		} else {
			x = event.pageX;
			y = event.pageY;
		}

		return {x:x, y:y};
	};


	T.fn.select_mode_timeTable = function (event){
		var date = $(this).data("date");
		console.log("## select_mode_timeTable ## " + date.format("llll"));
	};


	/*
	 * 「登録中」の予約を選択した場合
	 */
	T.fn.pre_reserve_band = function (el) {
		console.log("## pre_reserve_band ##");
		var date = el.data('rsvdate');
		var staytime = el.data('rsvstaytime');
		var reserve_data = {
			start_date: el.data('rsvdate'),
			staytime : el.data('rsvstaytime'),
			seats: [],
		}

		var prsvSheetid = '';
		$('.pre_reserve').each(function(){
			var left = $(this).position().left;
			var top  = $(this).position().top;

			//座席の取得
			var seat_idx1 = Math.floor(top / T.OPTION.size.time_unit_height) + 1;
			var seat_idx2 = Math.floor((top + $(this).outerHeight()) / T.OPTION.size.time_unit_height);

			var bound = T.status.seat_count.prereserve + T.status.seat_count.reserve
			if ( seat_idx1 > bound  || seat_idx2 > bound ) {
				alert('有効範囲を超えた座席がございます。');
				T.status.event_flg = 'nodrag';
				$('.pre_reserve').remove();
				return false;
			}

			if (seat_idx1 <= T.status.seat_count.prereserve) {
				reserve_data.seats.push(T.data.seats.prereserve[T.status.seat_count.prereserve - 1]);
			} else {
				var start_idx = seat_idx1 <= T.status.seat_count.prereserve ? T.status.seat_count.prereserve + 1 : seat_idx1;
				for (var i=start_idx;
					i <= seat_idx2 ; i++) {
					reserve_data.seats.push(T.data.seats.reserve[i-T.status.seat_count.prereserve-1]);
				};
			}
		});

		T.preReserveData.start_date = reserve_data.start_date.format("YYYY-MM-DD HH:mm");
		T.preReserveData.staytime   = reserve_data.staytime;
		T.preReserveData.seats = [];
		for (var i=0; i<reserve_data.seats.length; i++) {
			T.preReserveData.seats.push( reserve_data.seats[i].seatID );
		}


		//console.log(T.preReserveData);
		$('#date_time').html(T.preReserveData.start_date + "<br>滞在時間：" + T.preReserveData.staytime + "分");
		$('#telno').val("");
		$('#kname').val("");
		$('#persons').val("");
		$('#mail_ad').val("");
		$('#comment').val("");
		$("#dialog_cancel").hide();
		T.part.$dialog.dialog( "open" );
		T.status.event_flg = 'nodrag';
	}


	T.fn.select_start_TimeTable = function (event) {
		if ( $('.pre_reserve').length > 0 ) {
			return true; //登録中の予約が存在する場合
		}
		console.log("## select_start_TimeTable ##");

		var target = $(this);
		if ( !T.status.select_action_holder.b_move ) {
			var id = target.data('sheetnum');
			var idx = target.data('sheetidx');

			var offset = target.parent().parent().parent().parent().offset();

			var select_pos = T.fn.get_select_pos_TimeTable(event);
			x = select_pos.x;
			y = select_pos.y;
			var date = target.data('date');

			var rsvtimehour = date.get('hour');
			var rsvtimemin = date.get('minute');
			var openstrhour = T.OPTION.time_table_values.time_start;

			if ( T.data.current_date.get('date') != date.get('date') ) {
				rsvtimehour = rsvtimehour + 24;
			}

			T.status.select_action_holder.start = {
				 seat_id: id
				,seat_idx : idx
				,x: x
				,y: y
				,rsvdate: moment(date.toDate())
			};

			var topv = T.OPTION.size.time_unit_height*idx + 2;

			var leftv = T.OPTION.size.time_unit_width*(((rsvtimehour*60+rsvtimemin)-(openstrhour*60))/T.OPTION.time_table_values.time_interval);
			T.status.select_action_holder.$ghost_data_item       = $('<div />').addClass('pre_reserve npre_reserve').attr('id', 'pre_reserve');
			T.status.select_action_holder.$ghost_data_item_inner = '<div class="pre_reserve_inner shake multi_select" style=""><span></span><div>';
			T.status.select_action_holder.$ghost_data_item.append(T.status.select_action_holder.$ghost_data_item_inner);
			$('#tt_body_inner').append(T.status.select_action_holder.$ghost_data_item);

			T.status.select_action_holder.$ghost_data_item.css({top: topv + 'px', left: leftv + 'px'});

			T.status.select_action_holder.b_move = true;

		}
	}


	T.fn.select_move_TimeTable = function (event) {
		if (T.status.select_action_holder.b_move) {
			console.log("## select_move_TimeTable ##");

			if ($(this).hasClass('reserve') || $(this).hasClass('pre_reserve')) {
				alert('既に予約登録されています。');
				T.status.select_action_holder.$ghost_data_item.remove();
				T.status.select_action_holder.b_move = false;
				return;
			}

			var current_pos = T.fn.get_select_pos_TimeTable(event);
			var width  = current_pos.x - T.status.select_action_holder.start.x+10;
			var height = current_pos.y - T.status.select_action_holder.start.y+10;

			T.status.select_action_holder.$ghost_data_item.width(width);
			T.status.select_action_holder.$ghost_data_item.height(height);
			T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').width(width);
			T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').height(height);

		}
	}


	T.fn.select_end_TimeTable = function (event) {
		if (T.status.select_action_holder.b_move) {
			console.log("## select_end_TimeTable ##");

			if ( ( $(this).hasClass('reserve') || $(this).hasClass('pre_reserve') ) && !$(this).hasClass('npre_reserve')) {
				alert('既に予約登録されています。');
				T.status.select_action_holder.$ghost_data_item.remove();
				T.status.select_action_holder.b_move = false;
				return;
			}

			var select_pos = T.fn.get_select_pos_TimeTable(event);
			T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').children('span').text('登録中');

			var _width = T.status.select_action_holder.$ghost_data_item.width();
			var width = Math.ceil(_width / T.OPTION.size.time_unit_width ) * T.OPTION.size.time_unit_width;
			var rsvstaytime = Math.ceil( width / T.OPTION.size.time_unit_width ) * T.OPTION.time_table_values.time_interval;
			var _height = T.status.select_action_holder.$ghost_data_item.height();
			var sheetnum = Math.ceil( (T.status.select_action_holder.$ghost_data_item.height()) / T.OPTION.size.time_unit_height );
			var height = sheetnum * T.OPTION.size.time_unit_height;

			if ( width > T.OPTION.size.time_unit_width) {
				//StartとEndがある場合
				;
			} else {
				//StartとEndが同じである場合
				width = Math.ceil(8) * T.OPTION.size.time_unit_width;
				rsvstaytime = Math.ceil( width / T.OPTION.size.time_unit_width ) * T.OPTION.time_table_values.time_interval;
				height = T.OPTION.size.time_unit_height;
			}

			if ( width > T.OPTION.size.time_unit_width && height > 0) {
				T.status.select_action_holder.$ghost_data_item.width(width);
				T.status.select_action_holder.$ghost_data_item.height(height);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').width(width);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').height(height);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').children('span').width(width);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').children('span').height(height);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').children('span').css('line-height',height+'px');

				T.status.select_action_holder.$ghost_data_item.data('rsvdate',T.status.select_action_holder.start.rsvdate);
				T.status.select_action_holder.$ghost_data_item.data('rsvstaytime',rsvstaytime);
//				T.status.select_action_holder.$ghost_data_item.data('rsvstaytime',rsvstaytime);

				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').data('rsvdate',T.status.select_action_holder.start.rsvdate);
				T.status.select_action_holder.$ghost_data_item.children('.pre_reserve_inner').data('rsvstaytime',rsvstaytime);

				$('.pre_reserve').startFlashing();
				$('.pre_reserve').removeClass('npre_reserve');

				T.status.event_flg = 'nodrag';
				T.status.select_action_holder.b_move = false;
			} else {
				T.status.event_flg = 'nodrag';
				T.status.select_action_holder.b_move = false;
				$('.pre_reserve').remove();
			}
		}
	}


	var flashing = '';
	cancelFlashing = function(){
		if (flashing) {
			clearInterval(flashing);
		}
	};
	$.fn.startFlashing = function(){
		cancelFlashing();
		var target = $(this).attr('id');
		flashing = setInterval(function(){
						$('.'+target,parent.document).toggleClass('rsvflash');
				   },500)
	};

	T.fn.setCurrentDate = function(current_date) {
		console.log("## setCurrentDate ##");
		if (moment.locale() !== "ja")
			moment.locale("ja");
		var currentDate = moment(current_date);
		if (!currentDate.isValid()) {
			currentDate = moment();
		}
		T.data.current_date = currentDate;

		$("#current_date").text(T.data.current_date.format("ll dddd"));
	}

	T.fn.settingTimecell = function() {
		console.log("## settingTimecell ##");
		var dateStr = T.data.current_date.format("YYYYMMDD");
		$("td.time").each(function () {
			var hourClass = $(this).attr("class").match(/hour+\d+/);
			if (hourClass != null) {
				var date = moment(dateStr + hourClass, "YYYYMMDD[hour]HHmm");
				if (!date.isValid()) {
					var hourmin = hourClass.toString().match(/\d+/);
					var hour = hourmin.toString().substr(0, 2);
					var min = hourmin.toString().substr(2, 2);
					date = moment(dateStr, "YYYYMMDD");
					date.set("hour", hour);
					date.set("minute", min);
				}
				$(this).data("date", date);
			}
			var seatClass = $(this).attr("class").match(/[seat|seatx]+\d+/);
			if (seatClass != null) {
				var index = 0;
				if (seatClass.toString().startsWith("seatx")) {
					var count = parseInt(seatClass.toString().replace("seatx", ""));
					index = count - 1;
				} else {
					var count = parseInt(seatClass.toString().replace("seat", ""));
					index = T.status.seat_count.prereserve + count - 1;
				}
				$(this).data("sheetnum", seatClass.toString());
				$(this).data("sheetidx", index);
			}

		});
	};

	T.fn.reserve_band_single = function (e) {
		T.status.clicks = T.status.clicks + 1;
		var rsvid = $(this).data('rsvid');
		if( T.status.clicks == 1 && T.status.event_flg == 'nodrag') {
			if ( !T.fn.displayCheck() ) {
				return false;
			}
			T.fn.subWindowDisplayFlgSetting(500);

			T.status.timer = setTimeout(function() {
				T.fn.click_reserve_band(rsvid, true);
				T.status.clicks = 0;
			}, 700);
		} else {
			T.fn.subWindowDisplayFlgtrue();
			clearTimeout(T.status.timer);
			T.status.clicks = 0;
		}
	}
	T.fn.reserve_band_double = function (e) {
		if(T.status.event_flg == 'nodrag') {
			var rsvid = $(this).data('rsvid');
			e.preventDefault();
			T.fn.click_reserve_band(rsvid, false);
		}
	}

	T.fn.click_reserve_band = function(rsvid, single) {
		if ( !T.fn.displayCheck() ) {
			return false;
		}
		T.fn.subWindowDisplayFlgSetting(500);
		var reserveData = T.fn.getReserveDataById(rsvid);
		if (single) {
			T.OPTION.onClickReserve(reserveData);
		} else {
			T.OPTION.onDbClickReserve(reserveData);
		}
	}

	T.fn.load_reserve_info = function() {
		console.log("## load_reserve_info ##");
		$("div.reserve").remove();
		$("#seat_list_inner table.prereserve tr").each(function(index, tr){
			if (index > 0) {
				tr.remove();
				T.status.seat_count.prereserve -= 1;
				T.data.seats.prereserve.pop();
			}
		});
		$("#tt_body_inner table.prereserve tr").each(function(index, tr){
			if (index > 0) {
				tr.remove();
			}
		});
		$.each(T.data.reserve_list, function(index, reserve_data) {
			var date = moment(reserve_data.start_date);
			var rsvtimehour = date.get('hour');
			var rsvtimemin = date.get('minute');
			var staytime = reserve_data.staytime;
			var openstrhour = T.OPTION.time_table_values.time_start;

			if ( T.data.current_date.get('date') != date.get('date') ) {
				rsvtimehour = rsvtimehour + 24;
			}

			var infos = T.fn.separateSeats(reserve_data.seats);

			$.each(infos, function(i, info) {
				var topv = T.OPTION.size.time_unit_height * (info.seatPos - 1);
				var leftv = T.OPTION.size.time_unit_width*(((rsvtimehour*60+rsvtimemin)-(openstrhour*60))/T.OPTION.time_table_values.time_interval);
				var widthv = (staytime/T.OPTION.time_table_values.time_interval) * (T.OPTION.size.time_unit_width);
				var heightv = T.OPTION.size.time_unit_height * info.seatLength;
				var hasChild = infos.length > 1 ? true : false;
				T.fn.append_reserve_info(reserve_data, topv, leftv, widthv, heightv, info, hasChild);
			});
		});
		$('div.reserve').draggable({
			grid: [T.OPTION.size.time_unit_width,T.OPTION.size.time_unit_height],
			start: function(e, ui){
				T.fn.reserve_move_start($(this),e,ui);
			},
			drag: function(e, ui){
				T.fn.reserve_move_drag($(this),e,ui);
			},
			stop: function(e, ui){
				T.fn.reserve_move_exe($(this),e,ui,'drag');
			}
		});
		$('div.reserve.child').draggable('destroy');

		$('div.reserve').resizable({
			grid: [T.OPTION.size.time_unit_width,T.OPTION.size.time_unit_height],
			handles: "e,se,s",
			start: function(e, ui){
				T.fn.reserve_move_start($(this),e,ui);
			},
			resize: function(e, ui){
				T.fn.reserve_resizing($(this),e,ui);
			},
			stop: function(e, ui) {
				T.fn.reserve_move_exe($(this),e,ui,'resize');
			}
		});
		$('div.reserve.child').resizable('destroy');
	};

	T.fn.reserve_move_start = function (tg, event, ui) {
		console.log("## reserve_move_start ##");
		T.fn.clear_drag_scroll();
		T.status.dragging = true;
		$('.rsvDetail', ui.helper).hide();

		T.status.event_flg = 'edit';
		T.status.reserveMoveStartTop = ui.helper.position().top;
		T.status.reserveMoveStartLeft =  ui.helper.position().left;
		T.status.reserveMoveStartHeight = ui.helper.outerHeight(true);
		T.status.reserveMoveStartWidth = ui.helper.outerWidth(true);
		T.status.reserveMoveStartNameWidth = tg.find('.namearea').width();

		tg.css('overflow','hidden');

	}

	T.fn.dragScrollDown = function (){
		console.log("## dragScrollDown ##");
		var pos = T.part.$vs_move.position();
		var top = pos.top - T.OPTION.size.time_unit_height;
		if (top < T.OPTION.scroll.vert_max) {
			top = T.OPTION.scroll.vert_max;
		}

		T.part.$vs_move.css('top', top + 'px');

		var scroll_move_rate = top / T.OPTION.scroll.vert_max;
		if (scroll_move_rate > 1)
			scroll_move_rate = 1;
		T.part.$vs_sbar.css('top', Math.floor(scroll_move_rate * T.OPTION.scroll.vscroll_max) + 'px');
	}

	T.fn.dragScrollUp = function (){
		console.log("## dragScrollUp ##");
		var pos = T.part.$vs_move.position();
		var top = pos.top + T.OPTION.size.time_unit_height;
		if (top > 0) {
			top = 0;
		}

		T.part.$vs_move.css('top', top + 'px');

		var scroll_move_rate = top / T.OPTION.scroll.vert_max;
		if (scroll_move_rate > 1)
			scroll_move_rate = 1;
		T.part.$vs_sbar.css('top', Math.floor(scroll_move_rate * T.OPTION.scroll.vscroll_max) + 'px');
	}

	T.fn.dragScrollRight = function (){
		console.log("## dragScrollRight ##");
		var pos  = T.part.$hs.position();

		if (pos.left > T.OPTION.scroll.hor_max) {
			var setleft = pos.left - T.OPTION.size.time_unit_width;
			if (setleft < T.OPTION.scroll.hor_max) {
				setleft = T.OPTION.scroll.hor_max;
			}
			T.part.$hs.css('left', setleft + 'px');
		}
	}

	T.fn.dragScrollLeft = function() {
		console.log("## dragScrollLeft ##");
		var pos = T.part.$hs.position();

		if (pos.left < 0) {
			var setleft = pos.left + T.OPTION.size.time_unit_width;
			if (setleft > 0) {
				setleft = 0;
			}
			T.part.$hs.css('left', setleft + 'px');
		}
	}

	T.fn.clear_drag_scroll = function() {
		console.log("## clear_drag_scroll ##");
		if (T.OPTION.scrollInterval.$up) {
			clearInterval(T.OPTION.scrollInterval.$up);
			T.OPTION.scrollInterval.$up = null;
		}
		if (T.OPTION.scrollInterval.$down) {
			clearInterval(T.OPTION.scrollInterval.$down);
			T.OPTION.scrollInterval.$down = null;
		}
		if (T.OPTION.scrollInterval.$left) {
			clearInterval(T.OPTION.scrollInterval.$left);
			T.OPTION.scrollInterval.$left = null;
		}
		if (T.OPTION.scrollInterval.$right) {
			clearInterval(T.OPTION.scrollInterval.$right);
			T.OPTION.scrollInterval.$right = null;
		}

		T.OPTION.dragOffset.$top = 0;
		T.OPTION.dragOffset.$left = 0;
	};

	T.fn.reserve_move_drag = function (tg,e,ui) {
		console.log("## reserve_move_drag ##");
		if (ui.position.left < 0 && T.OPTION.dragOffset.$left == 0) {
			ui.position.left = 0;
		}
		if (ui.position.top < 0 && T.OPTION.dragOffset.$top == 0) {
			ui.position.top = 0;
		}
		ui.position.top += T.OPTION.dragOffset.$top;
		ui.position.left += T.OPTION.dragOffset.$left;

		var calDW = parseInt($('#tt_wrapper').width(),10);
		var calDH = parseInt($('#tt_wrapper').height(),10) - parseInt($('#tt_header').height(),10);
		var calAW = parseInt($('#tt').width(),10);
		var calAH = parseInt($('#tt_body_inner').height(),10);
		var calFL = parseInt($('#tt').position().left,10);
		var calFT = parseInt($('#tt_body_inner').position().top,10);
		var rsvH  = parseInt(tg.height(),10);
		var rsvW  = parseInt(tg.width(),10);

		var uheight = parseInt(T.OPTION.size.time_unit_height,10);
		var uwidth  = parseInt(T.OPTION.size.time_unit_width,10);
		var superiorEnd = uheight - calFT;
		var lowerEnd    = calDH - calFT - rsvH - uheight;
		var left = parseInt(ui.position.left,10);
		var top  = parseInt(ui.position.top,10);

		leftExtremity  = uwidth - calFL;
		rightExtremity = calDW - calFL - uwidth;

		if (lowerEnd < top) {
			if (!T.OPTION.scrollInterval.$down) {
				scroll_top = top;
				T.OPTION.scrollInterval.$down = setInterval(function(){
					T.fn.dragScrollDown();
					scroll_top = scroll_top + uheight;
					if ((calAH-rsvH) < scroll_top) {
						scroll_top = calAH - uheight;
						clearInterval(T.OPTION.scrollInterval.$down);
						T.OPTION.scrollInterval.$down = null;
					} else {
						T.OPTION.dragOffset.$top += uheight;
						tg.css('top', scroll_top + 'px');
					}
				}, T.OPTION.dragScrollSpeedSetting);
			}
		} else {
			if (T.OPTION.scrollInterval.$down) {
				clearInterval(T.OPTION.scrollInterval.$down);
				T.OPTION.scrollInterval.$down = null;
			}
		}

		if (superiorEnd > top) {
			if (!T.OPTION.scrollInterval.$up) {
				scroll_top = top;
				T.OPTION.scrollInterval.$up = setInterval(function(){
					T.fn.dragScrollUp();
					scroll_top = scroll_top - uheight;
					if ( scroll_top < 0 ) {
						clearInterval(T.OPTION.scrollInterval.$up);
						T.OPTION.scrollInterval.$up = null;
					} else {
						T.OPTION.dragOffset.$top -= uheight;
						tg.css('top', scroll_top + 'px');
					}
				}, T.OPTION.dragScrollSpeedSetting);
			}
		} else {
			if (T.OPTION.scrollInterval.$up) {
				clearInterval(T.OPTION.scrollInterval.$up);
				T.OPTION.scrollInterval.$up = null;
			}
		}

		if (rightExtremity < (left + rsvW)) {
			if (!T.OPTION.scrollInterval.$right) {
				scroll_left = left;
				T.OPTION.scrollInterval.$right = setInterval(function(){
					T.fn.dragScrollRight();
					scroll_left = scroll_left + uwidth;
					if ((scroll_left + rsvW) > calAW) {
						clearInterval(T.OPTION.scrollInterval.$right);
						T.OPTION.scrollInterval.$right = null;
					} else {
						T.OPTION.dragOffset.$left += uwidth;
						tg.css('left', scroll_left + 'px');
					}
				}, T.OPTION.dragScrollSpeedSetting);
			}
		} else {
			if (T.OPTION.scrollInterval.$right) {
				clearInterval(T.OPTION.scrollInterval.$right);
				T.OPTION.scrollInterval.$right = null;
			}
		}

		if (leftExtremity > left && left > 0) {
			if (!T.OPTION.scrollInterval.$left) {
				scroll_left = left;
				T.OPTION.scrollInterval.$left = setInterval(function(){
					T.fn.dragScrollLeft();
					scroll_left = scroll_left - uwidth;
					if (scroll_left < 0) {
						clearInterval(T.OPTION.scrollInterval.$left);
						T.OPTION.scrollInterval.$left = null;
					} else {
						T.OPTION.dragOffset.$left -= uwidth;
						tg.css('left', scroll_left + 'px');
					}
				}, T.OPTION.dragScrollSpeedSetting);
			}
		} else {
			if (T.OPTION.scrollInterval.$left) {
				clearInterval(T.OPTION.scrollInterval.$left);
				T.OPTION.scrollInterval.$left = null;
			}
		}
	}

	T.fn.rsvmoveCancel = function(tg){
		console.log("## rsvmoveCancel ##");

		tg.css('top', T.status.reserveMoveStartTop+'px');
		tg.css('left', T.status.reserveMoveStartLeft+'px');
		tg.css('height', T.status.reserveMoveStartHeight+'px');
		tg.children('span').css('height', T.status.reserveMoveStartHeight+'px');
		tg.children('span').css('line-height', T.status.reserveMoveStartHeight+'px');
		tg.css('width', T.status.reserveMoveStartWidth+'px');
		tg.find('.namearea').css('width', T.status.reserveMoveStartNameWidth+'px');
		tg.find('.inner').css('width', T.status.reserveMoveStartWidth+'px');
		tg.find('.inner').css('height',T.status.reserveMoveStartHeight+'px');
		tg.find('.inner').parent('span').css('width', T.status.reserveMoveStartWidth+'px');
		tg.find('.namearea').css('height', T.status.reserveMoveStartHeight+'px');
		tg.find('.namearea').css('line-height', T.status.reserveMoveStartHeight+'px');
		tg.find('.iconarea').css('height', T.status.reserveMoveStartHeight+'px');
		tg.find('.iconarea').css('line-height', T.status.reserveMoveStartHeight+'px');
		T.fn.loadingunDisplay();
		T.status.event_flg = 'nodrag';
		tg.css('overflow','inherit');
	}

	T.fn.reserve_resizing = function (tg, event, ui) {
		console.log("## reserve_resizing ##");

		height = tg.outerHeight();
		width = tg.outerWidth();

		var imgCnt = tg.find('.iconarea').attr('data-iconNum');
		imgCnt = parseInt(imgCnt);
		var iconWidth = T.fn.getIconWidth(imgCnt);

		nameWidth = ( parseInt( width, 10 ) - (parseInt( iconWidth , 10 )) );

		var nameWidth = parseInt(width,10) - parseInt(iconWidth,10);
		if ( nameWidth < 0 ) {
			nameWidth = 0;
		}

		tg.find('span').css('height',height+'px');
		tg.find('span').css('line-height',height+'px');
		tg.find('span').css('width',width+'px');
		tg.find('span').css('width',width+'px');
		tg.find('.namearea').css('width',nameWidth+'px');
		tg.find('.namearea').css('width',nameWidth+'px');
		tg.find('.iconarea').css('height',height+'px');
		tg.find('.iconarea').css('line-height',height+'px');
	}

	T.fn.reserve_move_exe = function (tg, event, ui, actionFlg) {
		console.log("## reserve_move_exe ##");

		T.fn.clear_drag_scroll();
		T.status.dragging = false;
		if ( !T.fn.displayCheck() ) {
			tg.css('overflow','inherit');
			return false;
		}
		T.fn.subWindowDisplayFlgSetting(500);

		try {
			var preRsvDate      = moment(tg.data('rsvdate'));
			var preRsvtimehour  = preRsvDate.get('hour');
			var preRsvtimemin   = preRsvDate.get('minute');
			var preRsvstaytime  = tg.data('rsvstaytime');
			var preRsvseats     = tg.data('seats');
			var preChildseats   = tg.data('childseats');
			var rsvid= tg.data('rsvid');
			var left = parseInt(ui.position.left,10);
			var top  = parseInt(ui.position.top,10);

			if (left < 0)
				left = 0;
			if (top  < 0)
				top  = 0;

			var openstrhour = T.OPTION.time_table_values.time_start;
			var openstrmin  = 0;
			var openendhour = T.OPTION.time_table_values.time_end;
			var openendmin  = 0
			var start_time = Math.ceil(left/T.OPTION.size.time_unit_width) * T.OPTION.time_table_values.time_interval + openstrhour * 60;
			var start_hour = Math.floor(start_time / 60);
			var start_min = Math.floor(start_time % 60);
			var rsvtimehour = start_hour;
			var rsvtimemin = start_min;
			var rsvstaytime=Math.ceil(Math.ceil(tg.outerWidth() / T.OPTION.size.time_unit_width) * T.OPTION.time_table_values.time_interval);
			var end_time = start_time + rsvstaytime;
			if ( openendhour < openstrhour ) {
				openendhour = openendhour + 24;
			}
			var shop_end_time = openendhour * 60 + openendmin;

			if ( shop_end_time <= start_time ) {
				alert('エラー： 閉店時間以降に予約時間を変更できません。');
				T.fn.rsvmoveCancel(tg);
				return false;
			}

			//座席の取得
			var seat_idx1 = Math.floor(top / T.OPTION.size.time_unit_height) + 1;
			var seat_idx2 = Math.round((top + tg.outerHeight()) / T.OPTION.size.time_unit_height);
			var all_seat_count = T.status.seat_count.prereserve + T.status.seat_count.reserve;
			if ( seat_idx1 > all_seat_count || seat_idx2 > all_seat_count ) {
				alert('有効範囲を超えています。');
				T.fn.rsvmoveCancel(tg);
				return false;
			}

			var setrsvStart = T.data.current_date.clone();

			if ( rsvtimehour > 23 ) {
				setrsvStart.add(1, 'days');
				setrsvStart.set('hour', rsvtimehour - 24);
			} else {
				setrsvStart.set('hour', rsvtimehour);
			}
			setrsvStart.set('minute', rsvtimemin);
			setrsvStart.set('second', 0);

			var setrsvEnd = setrsvStart.clone().add(rsvstaytime, 'minutes');

			var prereserveFlg = false;
			var isDuplicated = false;

			if (seat_idx1 <= T.status.seat_count.prereserve) {
				prereserveFlg = true;
			}

			if (!prereserveFlg) {
				$.each(T.data.reserve_list, function() {
					if (this.reserve_id != rsvid) {
						var start = moment(this.start_date);
						var end = start.clone().add(this.staytime, 'minutes');
						if (setrsvStart.isBetween(start, end, 'minute', '[)') ||
								setrsvEnd.isBetween(start, end, 'minute', '(]')) {
							for (var i=seat_idx1; i<=seat_idx2; i++) {
								if (this.seats.includes(T.fn.searchSeatInfo(i).seatID)) {
									isDuplicated = true;
									return;
								}
							}
							if (preChildseats.length > 0) {
								for (var j=0; j<preChildseats.length; j++) {
									if (this.seats.includes(preChildseats[j])) {
										isDuplicated = true;
										return;
									}
								}
							}
						}
					}
				});
			}

			if (isDuplicated) {
				alert('エラー 予約がはいっております。');
				T.fn.rsvmoveCancel(tg);
				return false;
			}

			var updated = {
				reserve_id: rsvid,
				start_date: preRsvDate,
				staytime: preRsvstaytime,
				person: 3, child: 0, seats: preRsvseats
			};

			var original = T.fn.getReserveDataById(rsvid);
			var changes = {
				pre: original,
				start_date: null,
				staytime: null,
				seats: null
			};

			var isChangedFlg = false;
			if (original.start_date !== setrsvStart.format("YYYY-MM-DD HH:mm")) {
				changes.start_date = setrsvStart.format("YYYY-MM-DD HH:mm");
				isChangedFlg = true;
			}
			if (original.staytime !== rsvstaytime) {
				changes.staytime = rsvstaytime;
				isChangedFlg = true;
			}
			if (prereserveFlg) {
				// prereserve -> prereserve
				if (preRsvseats.length == 0) {

				// reserve -> prereserve
				} else {
					changes.seats = [];
					isChangedFlg = true;
				}
			} else {
				// prereserve -> reserve
				if (preRsvseats.length == 0) {
					for (var i=seat_idx1; i<=seat_idx2; i++) {
						changes.seats = [];
						changes.seats.push(T.fn.searchSeatInfo(i).seatID);
						isChangedFlg = true;
					}
				// reserve -> reserve
				} else {
					var allSeats = [];
					for (var i=seat_idx1; i<=seat_idx2; i++) {
						allSeats.push(T.fn.searchSeatInfo(i).seatID);
					}
					for (var i=0; i<preChildseats.length; i++) {
						if (!allSeats.includes(preChildseats[i]))
							allSeats.push(preChildseats[i]);
					}
					allSeats.sort();
					if (allSeats.toString() !== original.seats.toString()) {
						changes.seats = allSeats;
						isChangedFlg = true;
					}
				}
			}

			if (isChangedFlg && confirm("予約内容を変更しますか？")) {
				T.OPTION.onUpdateReserve(changes, tg, T.fn.updateReserveSuccessCallback, T.fn.updateReserveFailCallback);
			} else {
				T.fn.rsvmoveCancel(tg);
				return false;
			}
		} catch (e) {
			console.log(e);
			alert('予約情報の変更に失敗しました。' + e);
		}
	}

	T.fn.updateReserveSuccessCallback = function(changes) {
		console.log("## updateReserveSuccessCallback ##");

		if (changes.start_date != null) {
			changes.pre.start_date = changes.start_date;
		}
		if (changes.staytime != null) {
			changes.pre.staytime = changes.staytime;
		}
		if (changes.seats != null) {
			changes.pre.seats = changes.seats;
		}
		T.fn.load_reserve_info();
	};

	T.fn.updateReserveFailCallback = function(tg) {
		T.fn.rsvmoveCancel(tg);
	}


	T.fn.getReserveDataById = function(id) {
		console.log("## getReserveDataById ##");

		for (var i=0; i<T.data.reserve_list.length; i++) {
			var reserve = T.data.reserve_list[i];
			if (reserve.reserve_id === id)
				return reserve;
		}
		return null;
	}

	T.fn.separateSeats = function(seats) {
		console.log("## separateSeats ##");

		var seatInfo = {
			type: '',
			seatPos: 0,
			seatLength: 0,
			isChild: false,
			seatChildIds: []
		}
		if (seats.length == 0) {
			seatInfo.type = "prereserve";
			seatInfo.seatPos = T.status.seat_count.prereserve;
			seatInfo.seatLength = 1;
			return [seatInfo];
		} else {
			var sortedSeats = [];
			for (var i = 0; i < T.data.seats.reserve.length; i++) {
				var refseat = T.data.seats.reserve[i];
				for (var j=0; j<seats.length; j++) {
					if (seats[j] == refseat.seatID) {
						sortedSeats.push({seatID: refseat.seatID, index: i});
						break;
					}
				}
			}
			if (sortedSeats.length == 0)
				return null;

			var separate = function(seatList) {
				var groupList = [];
				var isContinued = false;
				for (var i = 0; i< seatList.length; i++) {
					if ((i+1) == seatList.length) {
						if (isContinued) {
							groupList[groupList.length-1].push(seatList[i]);
							break;
						} else {
							groupList.push([seatList[i]]);
							break;
						}
					} else {
						if (isContinued) {
							groupList[groupList.length-1].push(seatList[i]);
						} else {
							groupList.push([seatList[i]]);
						}
						if ((seatList[i].index + 1) == seatList[i+1].index) {
							isContinued = true;
						} else {
							isContinued = false;
						}
					}
				}
				return groupList;
			};

			var infos = [];
			$.each(separate(sortedSeats), function(index, group){
				var seatInfo = {
					type: 'reserve',
					seatPos: 0,
					seatLength: 0,
					isChild: false,
					seatChildIds: []
				}
				seatInfo.seatPos = T.status.seat_count.prereserve + group[0].index + 1;
				seatInfo.seatLength = group.length;
				if (index > 0) {
					seatInfo.isChild = true;
					$.each(group, function(i, g) {
						infos[0].seatChildIds.push(g.seatID);
					});
				}
				infos.push(seatInfo);
			});
			return infos;
		}
	};

	T.fn.append_reserve_info = function (reserve_data, topv, leftv, widthv, heightv, seatInfo, hasChild) {
		console.log("## append_reserve_info ##");

		var dispPerson = reserve_data.person;
		if (reserve_data.child > 0) {
			dispPerson += '(' + reserve_data.child + ')';
		}
		var rsvgroupid = "";
		if (hasChild) {
			rsvgroupid = reserve_data.reserve_id;
		}
		var name = reserve_data.kname;
		var rsvstscolorcd = "blue";
		if (seatInfo.type === "prereserve") {
			rsvstscolorcd = "orange";
		} else if (seatInfo.isChild) {
			rsvstscolorcd = "#99aef9";
		}

		var reserve_info = $('<div id="rsv' + reserve_data.reserve_id
				 + '" class="reserveList' + reserve_data.reserve_id + ' reserve '
				 + (seatInfo.type === "prereserve" ? ' prereserve ' : '')
				 + (seatInfo.isChild ? ' child ' : '')
				 + '" style="top:' + topv + 'px;left:' + leftv + 'px;width:' + widthv + 'px;height:' + heightv + 'px;background-color:' + rsvstscolorcd + ';">');
		reserve_info.data('rsvid',      reserve_data.reserve_id);
		reserve_info.data('rsvgroupid', reserve_data.reserve_id);
		reserve_info.data('rsvdate',    reserve_data.start_date);
		reserve_info.data('person',     reserve_data.person);
		reserve_info.data('personchild',reserve_data.child);
		reserve_info.data('rsvstaytime',reserve_data.staytime);
		reserve_info.data('seats',      reserve_data.seats);
		reserve_info.data('childseats', seatInfo.seatChildIds);

		reserve_info.append(
				'<span style="height:' + heightv+'px;line-height:' + heightv + 'px;width:' + widthv + 'px;">'
			 	+ '<span class="inner" style="height:' + heightv + 'px;line-height:' + heightv + 'px;width:' + widthv+'px;">'
			 	+ '<span class="namearea" style="width:100%;">'
			 		+ '<div>' + name + '様 [' + dispPerson + '名]</div>' //reserve_id is too long, --> slice(-4)
			 	+ '</span>'
			 	+ '</span>'
			 	+ '</span>');
		$(reserve_info).appendTo('#tt_body_inner');
		if (seatInfo.type === "prereserve") {
			T.fn.addPrereserveRow();
		}
	}

	T.fn.addPrereserveRow = function() {
		console.log("## addPrereserveRow ##");

		var index = parseInt(T.data.seats.prereserve[T.status.seat_count.prereserve -1].seatName.replace("x", ""));
		index += 1;
		T.data.seats.prereserve.push({seatID: "", seatName: "x" + index, seatDesc: ""});
		T.status.seat_count.prereserve += 1;
		var tr = $("#seat_list table.prereserve tr").last();
		var vclass = tr.find("td").first().attr('class');
		var svclass = vclass.split(/[\s]/);
		var oldClass = svclass[0];
		var newClass =  "seatx" + index;
		var newTr = tr.clone();
		newTr.find("td").each(function(){
			var classs = $(this).attr('class').replace(oldClass, newClass);
			$(this).attr('class', classs);
			$(this).text("");
		});
		tr.after(newTr);

		tr = $("#tt_body_inner table.prereserve tr").last();
		newTr = tr.clone();
		newTr.find("td").each(function(){
			var classs = $(this).attr('class').replace(oldClass, newClass);
			$(this).attr('class', classs);
		});
		tr.after(newTr);
		$("div.reserve").not("div.prereserve").each(function() {
			var newTop = ($(this).position().top + T.OPTION.size.time_unit_height) + "px";
			$(this).css("top", newTop);
		});
		T.fn.settingTimecell();
		T.fn.setSize();
	};

	T.fn.removePrereserveRow = function() {
		console.log("## removePrereserveRow ##");

		if (T.status.seat_count.prereserve == 1)
			return;

		T.data.seats.prereserve.pop();
		T.status.seat_count.prereserve -= 1;
		$("#seat_list table.prereserve tr").last().remove();
		$("#tt_body_inner table.prereserve tr").last().remove();
		$("div.reserve").not("div.prereserve").each(function() {
			var newTop = ($(this).position().top - T.OPTION.size.time_unit_height) + "px";
			$(this).css("top", newTop);
		});
		T.fn.setSize();
	};

	T.fn.getIconWidth = function(imgCnt){
		var iconWidth = 12 + (22 * imgCnt);
		return iconWidth;
	};

	T.fn.subWindowDisplayFlgSetting = function(holdtime){
		T.status.displayFlg = false;
		setTimeout(function(){
			T.status.displayFlg = true;
		},holdtime);
	};

	T.fn.subWindowDisplayFlgtrue = function(){
		T.status.displayFlg = true;
	};

	T.fn.displayCheck = function () {
		if ( T.status.displayFlg ) {
			return true;
		} else {
			return false;
		}
	};

	T.fn.loadingunDisplay = function() {

	};

	T.fn.searchSeatInfo = function(index) {
		if (index <= T.status.seat_count.prereserve) {
			return T.data.seats.prereserve[index - 1];
		} else if (index <= T.status.seat_count.reserve + T.status.seat_count.prereserve) {
			return T.data.seats.reserve[index - T.status.seat_count.prereserve - 1];
		}
		return null;
	}

	var methods = {
		init : function(options) {
			defaults = $.extend(true, T.OPTION, options);
			T.$this = $(this);
			T.data.seats.reserve = defaults.rows;
			T.data.seats.prereserve = [
				{seatID: "", seatName: "x1", seatDesc: ""},
			];
			T.fn.setCurrentDate(defaults.current_date);
			T.fn.initSettings();
			methods.load.apply(this);
		},
		load: function() {
			T.fn.makeTable();
			T.fn.initSize();
			$(window).resize(T.fn.setSize);
			T.fn.setActions();
			T.fn.settingTimecell();
			T.fn.makeReserveInfoDialog();
		},
		set: function(options) {
			$.extend(true, T.OPTION, options);
		},
		reserveList: function(reserveList) {
			T.data.reserve_list = [];
			for (var i=0; i< reserveList.length; i++) {
				reserveList[i].seats.sort();
				T.data.reserve_list.push(reserveList[i]);
			}
			methods.refresh.apply(this);
		},
		reserveListPush: function(reserveData) {
			T.data.reserve_list.push(reserveData);
			methods.refresh.apply(this);
		},
		refresh: function() {
			T.fn.load_reserve_info();
		}
	};

	$.fn.timeTable = function( method ) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.timeTable');
		}
	};
})(jQuery);