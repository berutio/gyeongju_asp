var current;  // 現在表示中のページ（インデックス番号）
var pages = ['before', 'current', 'after'];  // （1）ページのリスト

// （2）ページ遷移時に、現在のページ（インデックス番号）を取得
$(document).bind('pagechange', function(e, d) {
  current = pages.indexOf(d.toPage.attr('id'));
});

// （3）左スワイプ時に次ページに移動
$(':jqmData(role="page")').live('swipeleft', function(){
  if (current === pages.length - 1) {
    var path = pages[0];
  } else {
    var path = pages[current + 1];
  }
  $.mobile.changePage(path, { transition : 'slide' });
});

// （4）右スワイプ時に前ページ移動
$(':jqmData(role="page")').live('swiperight', function(){
  if (current === 0) {
    var path = pages[pages.length - 1];
  } else {
    var path = pages[current - 1];
  }
  $.mobile.changePage(path, { transition : 'slide', reverse: true });
});
