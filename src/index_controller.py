# -*- coding: utf-8 -*-
import os
#import logging
#import StringIO

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users

#For Transrate
from posmodel import *
webapp.template.register_template_library('filters.filter')


class MainApp(Helper):
    def get(self):
        #loginData = self.logincheck()
        loginData = ""
        lang_sel = G_LANG_SEL
        user = users.get_current_user()
        if user:
            loginData = {'url':users.create_logout_url("/"),
                         'user':user,
                         'shop_user':None,
                         'shop_info':None,
                         'url_linktext':'Logout'}
        ####
        title_string = u"在日本 慶州 郷友会"

        loginUrl = users.create_login_url("/")
        template_vals = {
                'lang':lang_sel,
                'title':title_string,
                'loginData':loginData,
                'loginUrl':loginUrl,
            }
        self.helpDispatch('init', 'main.html', template_vals)

    def post(self):
        user = users.get_current_user()

        lang_sel = self.request.get('lang_sel')
        objShopUser = get_mem_user(user)
        objShopUser.lang_sel = lang_sel
        objShopUser.put()
        del_mem_user(user)

        if user:
            loginData = {'url':users.create_logout_url("/"),
                         'user':user,
                         'shop_user':None,
                         'shop_info':None,
                         'url_linktext':'Logout'}
            if objShopUser:
                loginData['shop_user'] = objShopUser
                loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)
        ####
        asp_flag = [0,0,0,0,0,0,0,0,0]
        title_string = u"JoinPOS"

        loginUrl = users.create_login_url("/")
        template_vals = {
                'lang':loginData['shop_user'].lang_sel,
                'title':title_string,
                'loginData':loginData,
                'loginUrl':loginUrl,
                'asp_flag':asp_flag
            }
        self.helpDispatch('init', 'index.html', template_vals)


'''
'''
class GoogleVerify(Helper):
    def get(self):
        template_vals = ""

        self.helpDispatch("init", 'googlef8108259e5ef1aa6.html', template_vals)

'''
'''
class GoogleSiteMap(Helper):
    def get(self):
        server_url  = get_mem_servername()
        baseurl     = "http://" + server_url + "/"
        introUrl    = baseurl + "intro/"
        url_array = [
                     baseurl,
                     introUrl,
                     ]
        now = getJST()
        ymd = "%04d%02d%02d" % (now.year, now.month, 1)
        template_vals = {
                'url_array':url_array,
                'udate':ymd,
            }
        self.response.headers['Content-Type'] = 'application/xml'
        self.helpDispatch("init", 'sitemap.xml', template_vals)

app = webapp.WSGIApplication([
    ('/',     MainApp),
    ("/googlef8108259e5ef1aa6.html", GoogleVerify),
    ("/sitemap.xml",    GoogleSiteMap),
    ], debug=True)

